package com.example.pdv.SQLite;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.security.NoSuchAlgorithmException;

import static com.example.pdv.SQLite.PostContract.*;
import static com.example.pdv.Utils.*;


/** CLASSE PARA CRIACAO/MANIPULACAO DO BANCO DE DADOS **/
final class PostDbHelper extends SQLiteOpenHelper {
    /** DEFINICAO DE TIPOS **/
    private static final String INT_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String TEXT_TYPE = " TEXT";
    // Os dados do SQLite assumem, basicamente,
    // algum dos tres tipos definidos acima

    /** CONSTRAINTS **/
    private static final String CONSTRAINT = "CONSTRAINT ";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String FOREIGN_KEY = " FOREIGN KEY";
    private static final String REFERENCES = " REFERENCES ";
    private static final String NOT_NULL = " NOT NULL";
    private static final String UNIQUE = " UNIQUE";
    private static final String COMMA_SEP = ",";

    /** COMANDOS PARA GERAR AS TABELAS **/
    private static final String[] SQL_CREATE_POSTS =
    {
        "CREATE TABLE " + LOGINS.TABLE_NAME + "(" +
            LOGINS.COLUMN_USER + TEXT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            LOGINS.COLUMN_PASSWD + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            CONSTRAINT + "pk_" + LOGINS.TABLE_NAME + PRIMARY_KEY + "(" + LOGINS.COLUMN_USER + ")" +
        ");",

        "CREATE TABLE " + GRUPOS.TABLE_NAME + "(" +
            GRUPOS._ID + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            GRUPOS.COLUMN_NOME + TEXT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            CONSTRAINT + "pk_" + GRUPOS.TABLE_NAME + PRIMARY_KEY + "(" + GRUPOS._ID + ")" +
        ");",

        "CREATE TABLE " + PRODUTOS.TABLE_NAME + "(" +
            PRODUTOS.COLUMN_COD + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            PRODUTOS.COLUMN_GRUPO + INT_TYPE + NOT_NULL + COMMA_SEP +
            PRODUTOS.COLUMN_NOME + TEXT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            PRODUTOS.COLUMN_EAN + TEXT_TYPE + UNIQUE + COMMA_SEP +
            PRODUTOS.COLUMN_PRC_V + REAL_TYPE + NOT_NULL + COMMA_SEP +
            PRODUTOS.COLUMN_CUSTO + REAL_TYPE + COMMA_SEP +
            PRODUTOS.COLUMN_PERC_MAX_DESC + REAL_TYPE + COMMA_SEP +
            CONSTRAINT + "pk_" + PRODUTOS.TABLE_NAME + PRIMARY_KEY + "(" + PRODUTOS.COLUMN_COD + ")" + COMMA_SEP +
            CONSTRAINT + "fk_" + PRODUTOS.TABLE_NAME + GRUPOS.TABLE_NAME +
                FOREIGN_KEY + "(" + PRODUTOS.COLUMN_GRUPO + ")" +
                REFERENCES + GRUPOS.TABLE_NAME + "(" + GRUPOS._ID + ")" +
        ");",

        "CREATE TABLE " + VENDAS.TABLE_NAME + "(" +
            VENDAS.COLUMN_COD + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            VENDAS.COLUMN_DATA + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            VENDAS.COLUMN_HORA + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            VENDAS.COLUMN_VALOR + REAL_TYPE + COMMA_SEP +
            VENDAS.COLUMN_ABERTA + INT_TYPE + NOT_NULL + COMMA_SEP +
            VENDAS.COLUMN_CANCELADA + INT_TYPE + COMMA_SEP +
            CONSTRAINT + "pk_" + VENDAS.TABLE_NAME + PRIMARY_KEY + "(" + VENDAS.COLUMN_COD + ")" +
        ");",

        "CREATE TABLE " + PRODUTOS_VENDAS.TABLE_NAME + "(" +
            PRODUTOS_VENDAS._ID + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            PRODUTOS_VENDAS.COLUMN_CODPROD + INT_TYPE + NOT_NULL + COMMA_SEP +
            PRODUTOS_VENDAS.COLUMN_CODVENDA + INT_TYPE + NOT_NULL + COMMA_SEP +
            PRODUTOS_VENDAS.COLUMN_QTDE + INT_TYPE + NOT_NULL + COMMA_SEP +
            PRODUTOS_VENDAS.COLUMN_DESC + REAL_TYPE + NOT_NULL + COMMA_SEP +
            CONSTRAINT + "pk_" + PRODUTOS_VENDAS.TABLE_NAME + PRIMARY_KEY + "(" +
                PRODUTOS_VENDAS._ID + ")" +
            CONSTRAINT + "fk_" + PRODUTOS_VENDAS.TABLE_NAME + PRODUTOS.TABLE_NAME +
                FOREIGN_KEY + "(" + PRODUTOS_VENDAS.COLUMN_CODPROD + ")" +
                REFERENCES + PRODUTOS.TABLE_NAME + "(" + PRODUTOS.COLUMN_COD + ")" +
            CONSTRAINT + "fk_" + PRODUTOS_VENDAS.TABLE_NAME + VENDAS.TABLE_NAME +
                FOREIGN_KEY + "(" + PRODUTOS_VENDAS.COLUMN_CODVENDA + ")" +
                REFERENCES + VENDAS.TABLE_NAME + "(" + VENDAS.COLUMN_COD + ")" +
        ");",

        "CREATE TABLE " + PAGAMENTOS.TABLE_NAME + "(" +
            PAGAMENTOS._ID + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            PAGAMENTOS.COLUMN_CODVENDA + INT_TYPE + NOT_NULL + COMMA_SEP +
            PAGAMENTOS.COLUMN_FPGTO + INT_TYPE + NOT_NULL + COMMA_SEP +
            PAGAMENTOS.COLUMN_VALOR + REAL_TYPE + NOT_NULL + COMMA_SEP +
            PAGAMENTOS.COLUMN_PARCELAS + INT_TYPE + COMMA_SEP +
            CONSTRAINT + "pk_" + PAGAMENTOS.TABLE_NAME + PRIMARY_KEY + "(" + PAGAMENTOS._ID + ")" +
            CONSTRAINT + "fk_" + PAGAMENTOS.TABLE_NAME + VENDAS.TABLE_NAME +
                FOREIGN_KEY + "(" + PAGAMENTOS.COLUMN_CODVENDA + ")" +
                REFERENCES + VENDAS.TABLE_NAME + "(" + VENDAS.COLUMN_COD + ")" +
        ");",

        "CREATE TABLE " + CAIXAS.TABLE_NAME + "(" +
            CAIXAS.COLUMN_COD + INT_TYPE + NOT_NULL + UNIQUE + COMMA_SEP +
            CAIXAS.COLUMN_DATABER + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            CAIXAS.COLUMN_HORABER + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            CAIXAS.COLUMN_VALORABER + REAL_TYPE + NOT_NULL + COMMA_SEP +
            CAIXAS.COLUMN_DATAFECH + TEXT_TYPE + COMMA_SEP +
            CAIXAS.COLUMN_HORAFECH + TEXT_TYPE + COMMA_SEP +
            CAIXAS.COLUMN_VENDABRUTA + REAL_TYPE + COMMA_SEP +
            CAIXAS.COLUMN_CANCELAMENTOS + REAL_TYPE + COMMA_SEP +
            CAIXAS.COLUMN_ABERTO + INT_TYPE + NOT_NULL + COMMA_SEP +
            CONSTRAINT + "pk_" + CAIXAS.TABLE_NAME + PRIMARY_KEY + "(" + CAIXAS.COLUMN_COD + ")" +
        ");",

        "CREATE TABLE " + VENDAS_CAIXAS.TABLE_NAME + "(" +
            VENDAS_CAIXAS.COLUMN_CODVENDA + INT_TYPE + NOT_NULL + COMMA_SEP +
            VENDAS_CAIXAS.COLUMN_CODCAIXA + INT_TYPE + NOT_NULL + COMMA_SEP +
            CONSTRAINT + "pk_" + VENDAS_CAIXAS.TABLE_NAME + PRIMARY_KEY + "(" +
                VENDAS_CAIXAS.COLUMN_CODVENDA + ", " + VENDAS_CAIXAS.COLUMN_CODCAIXA + ")" +
            CONSTRAINT + "fk_" + VENDAS_CAIXAS.TABLE_NAME + VENDAS.TABLE_NAME +
                FOREIGN_KEY + "(" + VENDAS_CAIXAS.COLUMN_CODVENDA + ")" +
                REFERENCES + VENDAS.TABLE_NAME + "(" + VENDAS.COLUMN_COD + ")" +
            CONSTRAINT + "fk_" + VENDAS_CAIXAS.TABLE_NAME + CAIXAS.TABLE_NAME +
                FOREIGN_KEY + "(" + VENDAS_CAIXAS.COLUMN_CODCAIXA + ")" +
                REFERENCES + CAIXAS.TABLE_NAME + "(" + CAIXAS.COLUMN_COD + ")" +
        ");"
    };

    /** COMANDOS PARA DROPAR AS TABELAS **/
    private static final String[] SQL_DELETE_POSTS =
    {
        "DROP TABLE IF EXISTS " + PRODUTOS_VENDAS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + VENDAS_CAIXAS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + PRODUTOS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + GRUPOS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + PAGAMENTOS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + VENDAS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + CAIXAS.TABLE_NAME,
        "DROP TABLE IF EXISTS " + LOGINS.TABLE_NAME
    };

    /** DEFINE ALGUMAS CONSTANTES... **/
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PDV.db";


    /*************************************************************************************************/
    /*************************************************************************************************/


    /** CONSTRUTOR **/
    PostDbHelper(Context context) {
        /*
         * Create a helper object to create, open, and/or manage a database. This method
         * always returns very quickly. The database is not actually created or opened
         * until one of getWritableDatabase() or getReadableDatabase() is called.
         */
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        /*
         * getWritableDatabase() :
         *
         * Create and/or open a database that will be used for reading and writing. The
         * first time this is called, the database will be opened and onCreate(SQLiteDatabase),
         * onUpgrade(SQLiteDatabase, int, int) and/or onOpen(SQLiteDatabase) will be called.
         */
    }

    /** IMPLEMENTACAO DOS METODOS ABSTRATOS **/
    public final void onCreate(SQLiteDatabase db) {
        /*
         * Called when the database is created for the first time. This is where the
         * creation of tables and the initial population of the tables should happen.
         */
        for (String sqlCreatePost : SQL_CREATE_POSTS) {
            db.execSQL(sqlCreatePost);
        }

        // O comando abaixo cria um login generico, para que o usuario possa acessar o PDV na
        // primeira vez que for usa'-lo. Este login deve ser REMOVIDO, no PDV, o mais rapido possivel!
        String username = "admin";
        String password = null;
        int flag = 0;

        try {
            password = toHex(getSHA256("admin"), true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            flag = 1;
        }

        if (flag == 0) {
            db.execSQL(
                    "INSERT INTO " + LOGINS.TABLE_NAME + " VALUES(" +
                    "'" + username + "'" + ", " + "'" + password + "'" +
                    ");"
            );
        }
    }

    public final void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
         * Called when the database needs to be upgraded. The implementation should use
         * this method to drop tables, add tables, or do anything else it needs to upgrade
         * to the new schema version. The SQLite ALTER TABLE documentation can be found
         * here: https://sqlite.org/lang_altertable.html
         */
        for (String sqlDeletePost : SQL_DELETE_POSTS){
            db.execSQL(sqlDeletePost);
        }
        onCreate(db);
    }
}
