package com.example.pdv.SQLite;


import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import static android.database.Cursor.*;


/** CLASSE DE ACESSO AO BANCO DE DADOS **/
public final class DAO {
    private PostDbHelper mDbHelper;


    /** ABRE CONEXAO COM BANCO DE DADOS **/
    public final void OPEN(Context context){
        mDbHelper = new PostDbHelper(context);
    }

    /** OPERACAO INSERT **/
    public final long INSERT(String table, String nullColumnHack, ContentValues values){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.execSQL("PRAGMA foreign_keys = ON");
        long newRowId = db.insert(table, nullColumnHack, values);
        db.close();

        return newRowId;
    }

    /** OPERACAO SELECT **/
    public final String[][] SELECT(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.execSQL("PRAGMA foreign_keys = ON");
        String[][] RESULT;

        Cursor c = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
        RESULT = new String[c.getCount()][c.getColumnCount()];

        for (int i = 0; i < c.getCount(); i++){
            c.moveToPosition(i);
            for (int j = 0; j < c.getColumnCount(); j++) {
                switch (c.getType(j)) {
                    case FIELD_TYPE_NULL:
                        RESULT[i][j] = null;
                        break;
                    case FIELD_TYPE_INTEGER:
                        RESULT[i][j] = Long.toString(c.getLong(j));
                        break;
                    case FIELD_TYPE_FLOAT:
                        RESULT[i][j] = Double.toString(c.getDouble(j));
                        break;
                    case FIELD_TYPE_STRING:
                        RESULT[i][j] = c.getString(j);
                        break;
                    case FIELD_TYPE_BLOB:
                        RESULT[i][j] = new String(c.getBlob(j));
                }
            }
        }

        c.close();
        db.close();

        return RESULT;
    }

    /** OPERACAO UPDATE **/
    public final int UPDATE(String table, ContentValues values, String whereClause, String[] whereArgs){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        db.execSQL("PRAGMA foreign_keys = ON");
        int rowsAffected = db.update(table, values, whereClause, whereArgs);
        db.close();

        return rowsAffected;
    }

    /** OPERACAO DELETE **/
    public final int DELETE(String table, String whereClause, String[] whereArgs){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.execSQL("PRAGMA foreign_keys = ON");
        int rowsAffected = db.delete(table, whereClause, whereArgs);
        db.close();

        return rowsAffected;
    }

    /** FECHA CONEXAO COM BANCO DE DADOS **/
    public final void CLOSE(){
        mDbHelper.close();
    }
}
