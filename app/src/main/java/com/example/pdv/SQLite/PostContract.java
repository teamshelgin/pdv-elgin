package com.example.pdv.SQLite;


import android.provider.BaseColumns;


/** CLASSE PARA DEFINIR AS TABELAS ENVOLVIDAS NO BANCO DE DADOS **/
public final class PostContract {

    private PostContract() {}

    /** TABELA LOGINS **/
    public static final class LOGINS implements BaseColumns {
        public static final String TABLE_NAME = "Logins";
        public static final String COLUMN_USER = "Username"; // TEXT PK
        public static final String COLUMN_PASSWD = "Password"; // TEXT
    }

    /** TABELA GRUPOS **/
    public static final class GRUPOS implements BaseColumns{
        public static final String TABLE_NAME = "Grupos";
        public static final String COLUMN_NOME = "Nome"; // TEXT
    }

    /** TABELA PRODUTOS **/
    public static final class PRODUTOS implements BaseColumns{
        public static final String TABLE_NAME = "Produtos";
        public static final String COLUMN_COD = "Codigo"; // INTEGER PK
        public static final String COLUMN_GRUPO = "Grupo"; // INTEGER FK
        public static final String COLUMN_NOME = "Nome"; // TEXT
        public static final String COLUMN_EAN = "EAN"; // TEXT
        public static final String COLUMN_PRC_V = "PrecoVenda"; // REAL
        public static final String COLUMN_CUSTO = "Custo"; // REAL
        public static final String COLUMN_PERC_MAX_DESC = "PercentualMaxDesconto"; // REAL
    }

    /** TABELA VENDAS **/
    public static final class VENDAS implements BaseColumns{
        public static final String TABLE_NAME = "Vendas";
        public static final String COLUMN_COD = "Codigo"; // INTEGER PK
        public static final String COLUMN_DATA = "Data"; // TEXT
        public static final String COLUMN_HORA = "Hora"; // TEXT
        public static final String COLUMN_VALOR = "Valor"; // REAL
        public static final String COLUMN_ABERTA = "Aberta"; // INTEGER (0 = FALSE, 1 = TRUE)
        public static final String COLUMN_CANCELADA = "Cancelada"; // INTEGER (0 = FALSE, 1 = TRUE)
    }

    /** TABELA PRODUTOS_VENDAS **/
    public static final class PRODUTOS_VENDAS implements BaseColumns{
        public static final String TABLE_NAME = "ProdutosVendas";
        public static final String COLUMN_CODPROD = "CodigoProduto"; // INTEGER FK
        public static final String COLUMN_CODVENDA = "CodigoVenda"; // INTEGER FK
        public static final String COLUMN_QTDE = "Quantidade"; // INTEGER
        public static final String COLUMN_DESC = "Desconto"; // REAL
    }

    /** TABELA PAGAMENTOS **/
    public static final class PAGAMENTOS implements BaseColumns{
        public static final String TABLE_NAME = "Pagamentos";
        public static final String COLUMN_CODVENDA = "CodigoVenda"; // INTEGER FK
        public static final String COLUMN_FPGTO = "FormaPagamento"; // INTEGER
        public static final String COLUMN_VALOR = "ValorPago"; // REAL
        public static final String COLUMN_PARCELAS = "nParcelas"; // INTEGER
    }

    /** TABELA CAIXAS **/
    public static final class CAIXAS implements BaseColumns{
        public static final String TABLE_NAME = "Caixas";
        public static final String COLUMN_COD = "Codigo"; // INTEGER PK
        public static final String COLUMN_DATABER = "DataAbertura"; // TEXT
        public static final String COLUMN_HORABER = "HoraAbertura"; // TEXT
        public static final String COLUMN_VALORABER = "ValorAbertura"; // REAL
        public static final String COLUMN_DATAFECH = "DataFechamento"; // TEXT
        public static final String COLUMN_HORAFECH = "HoraFechamento"; // TEXT
        public static final String COLUMN_VENDABRUTA = "VendaBruta"; // REAL
        public static final String COLUMN_CANCELAMENTOS = "Cancelamentos"; // REAL
        public static final String COLUMN_ABERTO = "Aberto"; // INTEGER (0 = FALSE, 1 = TRUE)
    }

    /** TABELA VENDAS_CAIXAS **/
    public static final class VENDAS_CAIXAS implements BaseColumns{
        public static final String TABLE_NAME = "VendasCaixas";
        public static final String COLUMN_CODVENDA = "CodigoVenda"; // INTEGER PK FK
        public static final String COLUMN_CODCAIXA = "CodigoCaixa"; // INTEGER PK FK
    }
}
