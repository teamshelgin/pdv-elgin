package com.example.pdv;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;

import com.example.pdv.Home.HomeActivity;
import static com.example.pdv.Utils.*;


public class LoginActivity extends AppCompatActivity {

    Intent intent;
    EditText username, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        intent = new Intent(this, HomeActivity.class);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validarLogin(getApplicationContext(), username.getText().toString(), password.getText().toString())){
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this, "Login inválido. Tente novamente.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
