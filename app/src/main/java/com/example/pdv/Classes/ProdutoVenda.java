package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;


/** CLASSE QUE REPRESENTA O RELACIONAMENTO ENTRE PRODUTOS E VENDAS **/
public class ProdutoVenda implements Parcelable {
    private int codigo; // ID
    private int codProd;
    private int codVenda;
    private int qtde;
    private double desconto;

    public ProdutoVenda(int codProd, int codVenda, int qtde, double desconto){
        this.codProd = codProd;
        this.codVenda = codVenda;
        this.qtde = qtde;
        this.desconto = desconto;
    }

    public int getCodigo() { return codigo; }
    public void setCodigo(int codigo) { this.codigo = codigo; }

    public int getCodProd() { return codProd; }
    public void setCodProd(int codProd) { this.codProd = codProd; }

    public int getCodVenda() { return codVenda; }
    public void setCodVenda(int codVenda) { this.codVenda = codVenda; }

    public int getQtde() { return qtde; }
    public void setQtde(int qtde) { this.qtde = qtde; }

    public double getDesconto() { return desconto; }
    public void setDesconto(double desconto) { this.desconto = desconto; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private ProdutoVenda(Parcel in) {
        codigo = in.readInt();
        codProd = in.readInt();
        codVenda = in.readInt();
        qtde = in.readInt();
        desconto = in.readDouble();
    }

    public static final Creator<ProdutoVenda> CREATOR = new Creator<ProdutoVenda>() {
        @Override
        public ProdutoVenda createFromParcel(Parcel in) {
            return new ProdutoVenda(in);
        }

        @Override
        public ProdutoVenda[] newArray(int size) {
            return new ProdutoVenda[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(codigo);
        parcel.writeInt(codProd);
        parcel.writeInt(codVenda);
        parcel.writeInt(qtde);
        parcel.writeDouble(desconto);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
