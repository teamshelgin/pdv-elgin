package com.example.pdv.Classes;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.pdv.SQLite.DAO;
import static com.example.pdv.SQLite.PostContract.*;


/** CLASSE QUE REPRESENTA UM PRODUTO **/
public class Produto implements Parcelable {
    private static final DAO bd = new DAO();

    private static String table, selection;
    private static String[] columns, selectionArgs;
    private static String[][] RESULT;

    // DADOS DO PRODUTO
    private int cod;
    private int grupo;
    private String nome;
    private String EAN;
    private double precoVenda;
    private double custo;
    private double percMaxDesc;

    public Produto(int cod, int grupo, String nome, double precoVenda){
        this.cod = cod;
        this.grupo = grupo;
        this.nome = nome;
        this.precoVenda = precoVenda;
    }

    public int getCod() { return cod; }
    public void setCod(int cod) { this.cod = cod; }

    public int getGrupo() { return grupo; }
    public void setGrupo(int grupo) { this.grupo = grupo; }

    public String getNome() { return nome; }
    public void setNome(String nome) { this.nome = nome; }

    public String getEAN() { return EAN; }
    public void setEAN(String EAN) { this.EAN = EAN; }

    public double getPrecoVenda() { return precoVenda; }
    public void setPrecoVenda(double precoVenda) { this.precoVenda = precoVenda; }

    public double getCusto() { return custo; }
    public void setCusto(double custo) { this.custo = custo; }

    public double getPercMaxDesc() { return percMaxDesc; }
    public void setPercMaxDesc(double percMaxDesc) { this.percMaxDesc = percMaxDesc; }


    // METODOS ESTATICOS
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    public static final int cod2grp(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_GRUPO
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // Codigo nao encontrado
        }else{
            return Integer.parseInt(RESULT[0][1]); // Retorna grupo do produto com o codigo recebido
        }
    }

    public static final String cod2nom(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_NOME
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return null; // Codigo nao encontrado
        }else{
            return RESULT[0][1].toUpperCase(); // Retorna nome do produto com o codigo recebido
        }
    }

    public static final String cod2ean(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_EAN
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return null; // Codigo nao encontrado
        }else{
            return RESULT[0][1]; // Retorna EAN do produto com o codigo recebido
        }
    }

    public static final double cod2prv(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_PRC_V
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // Codigo nao encontrado
        }else{
            return Double.parseDouble(RESULT[0][1]); // Retorna preco de venda do produto com o codigo recebido
        }
    }

    public static final double cod2cus(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_CUSTO
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // Codigo nao encontrado
        }else{
            return Double.parseDouble(RESULT[0][1]); // Retorna custo do produto com o codigo recebido
        }
    }

    public static final double cod2pmd(Context context, int cod){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_PERC_MAX_DESC
        };
        selection = PRODUTOS.COLUMN_COD + " = ?";
        selectionArgs = new String[]{Integer.toString(cod)};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // Codigo nao encontrado
        }else{
            return Double.parseDouble(RESULT[0][1]); // Retorna percentual max. de desconto do produto com o codigo recebido
        }
    }

    public static final int nom2cod(Context context, String nom){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_NOME
        };
        selection = PRODUTOS.COLUMN_NOME + " = ?";
        selectionArgs = new String[]{nom.toUpperCase()};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // Nome nao encontrado
        }else{
            return Integer.parseInt(RESULT[0][0]); // Retorna codigo do produto com o nome recebido
        }
    }

    public static final int ean2cod(Context context, String ean){
        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_EAN
        };
        selection = PRODUTOS.COLUMN_EAN + " = ?";
        selectionArgs = new String[]{ean};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        if (RESULT.length == 0){
            return 0; // EAN nao encontrado
        }else{
            return Integer.parseInt(RESULT[0][0]); // Retorna codigo do produto com o EAN recebido
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Produto(Parcel in) {
        cod = in.readInt();
        grupo = in.readInt();
        nome = in.readString();
        EAN = in.readString();
        precoVenda = in.readDouble();
        custo = in.readDouble();
        percMaxDesc = in.readDouble();
    }

    public static final Creator<Produto> CREATOR = new Creator<Produto>() {
        @Override
        public Produto createFromParcel(Parcel in) {
            return new Produto(in);
        }

        @Override
        public Produto[] newArray(int size) {
            return new Produto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(cod);
        parcel.writeInt(grupo);
        parcel.writeString(nome);
        parcel.writeString(EAN);
        parcel.writeDouble(precoVenda);
        parcel.writeDouble(custo);
        parcel.writeDouble(percMaxDesc);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
