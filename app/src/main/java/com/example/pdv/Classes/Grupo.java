package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;


/** CLASSE QUE REPRESENTA UM GRUPO DE PRODUTOS **/
public class Grupo implements Parcelable {
    private int codigo; // ID
    private String nome;

    public Grupo(String nome){
        this.nome = nome;
    }

    public int getCodigo() { return codigo; }
    public void setCodigo(int codigo) { this.codigo = codigo; }

    public String getNome() { return nome; }
    public void setNome(String nome) { this.nome = nome; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Grupo(Parcel in) {
        codigo = in.readInt();
        nome = in.readString();
    }

    public static final Creator<Grupo> CREATOR = new Creator<Grupo>() {
        @Override
        public Grupo createFromParcel(Parcel in) {
            return new Grupo(in);
        }

        @Override
        public Grupo[] newArray(int size) {
            return new Grupo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(codigo);
        parcel.writeString(nome);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
