package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;


/** CLASSE QUE REPRESENTA O RELACIONAMENTO ENTRE VENDAS E CAIXAS **/
public class VendaCaixa implements Parcelable {
    private int codVenda;
    private int codCaixa;

    public VendaCaixa(int codVenda, int codCaixa){
        this.codVenda = codVenda;
        this.codCaixa = codCaixa;
    }

    public int getCodVenda() { return codVenda; }
    public void setCodVenda(int codVenda) { this.codVenda = codVenda; }

    public int getCodCaixa() { return codCaixa; }
    public void setCodCaixa(int codCaixa) { this.codCaixa = codCaixa; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private VendaCaixa(Parcel in) {
        codVenda = in.readInt();
        codCaixa = in.readInt();
    }

    public static final Creator<VendaCaixa> CREATOR = new Creator<VendaCaixa>() {
        @Override
        public VendaCaixa createFromParcel(Parcel in) {
            return new VendaCaixa(in);
        }

        @Override
        public VendaCaixa[] newArray(int size) {
            return new VendaCaixa[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(codVenda);
        parcel.writeInt(codCaixa);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
