package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.Calendar;


/** CLASSE QUE REPRESENTA UMA DATA **/
public class Data implements Parcelable {
    public static final int MAX_ANO = 2099;
    private Calendar c = Calendar.getInstance();

    private int ano;
    private int mes;
    private int dia;

    public Data(){
        setAno(c.get(Calendar.YEAR));
        setMes(c.get(Calendar.MONTH) + 1);
        setDia(c.get(Calendar.DAY_OF_MONTH));
    }

    private boolean isBissexto(int ano){
        return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
    }

    public int getAno() { return ano; }
    public void setAno(int ano){
        if (ano >= 1 && ano <= MAX_ANO){
            this.ano = ano;
        }
    }

    public int getMes() { return mes; }
    public void setMes(int mes) {
        if (mes >= 1 && mes <= 12){
            this.mes = mes;
        }
    }

    public int getDia() { return dia; }
    public void setDia(int dia) {
        int numDias;

        switch (getMes()){
            case 2: numDias = isBissexto(getAno()) ? 29 : 28; break;
            case 4: case 6: case 9: case 11: numDias = 30; break;
            default: numDias = 31;
        }

        if (dia >= 1 && dia <= numDias){
            this.dia = dia;
        }
    }

    public String toString(){
        String dia, mes, ano = "";

        if (getDia() < 10){
            dia = "0" + getDia();
        }else{
            dia = Integer.toString(getDia());
        }

        if (getMes() < 10){
            mes = "0" + getMes();
        }else{
            mes = Integer.toString(getMes());
        }

        int tam = Integer.toString(getAno()).length();
        for (int i=tam; i<Integer.toString(MAX_ANO).length(); i++){
            ano += "0";
        } ano += getAno();

        return dia + "/" + mes + "/" + ano;
    }

    public static Data getData(String inData){
        int ano, mes, dia;
        Data data = new Data();

        if (inData == null) inData = ""; // Previne NullPointerException

        String[] outData = inData.split("\\/");
        if (outData.length != 3) return data;

        try{
            ano = Integer.parseInt(outData[2]);
            mes = Integer.parseInt(outData[1]);
            dia = Integer.parseInt(outData[0]);
        } catch (NumberFormatException e){
            e.printStackTrace();
            return data;
        }

        data.setAno(ano);
        data.setMes(mes);
        data.setDia(dia);
        return data;
    }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Data(Parcel in) {
        ano = in.readInt();
        mes = in.readInt();
        dia = in.readInt();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ano);
        parcel.writeInt(mes);
        parcel.writeInt(dia);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
