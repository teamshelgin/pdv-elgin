package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;


/** CLASSE QUE REPRESENTA UM PAGAMENTO **/
public class Pagamento implements Parcelable {
    // FORMAS DE PAGAMENTO DISPONIVEIS
    public static final int CARTAO_CREDITO = 1;
    public static final int CARTAO_DEBITO = 2;
    public static final int DINHEIRO = 3;
    public static final int CHEQUE = 4;
    public static final int CREDITO_LOJA = 5;
    public static final int VALE_ALIMENTACAO = 10;
    public static final int VALE_REFEICAO = 11;
    public static final int VALE_PRESENTE = 12;
    public static final int VALE_COMBUSTIVEL = 13;
    public static final int OUTROS = 99;

    private int codigo; // ID
    private int codVenda;
    private int fpgto;
    private double valorPago;
    private int parcelas;

    public Pagamento(int codVenda, int fpgto, double valorPago){
        this.codVenda = codVenda;
        this.fpgto = fpgto;
        this.valorPago = valorPago;
    }

    public int getCodigo() { return codigo; }
    public void setCodigo(int codigo) { this.codigo = codigo; }

    public int getCodVenda() { return codVenda; }
    public void setCodVenda(int codVenda) { this.codVenda = codVenda; }

    public int getFpgto() { return fpgto; }
    public void setFpgto(int fpgto) { this.fpgto = fpgto; }

    public double getValorPago() { return valorPago; }
    public void setValorPago(double valorPago) { this.valorPago = valorPago; }

    public int getParcelas() { return parcelas; }
    public void setParcelas(int parcelas) { this.parcelas = parcelas; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Pagamento(Parcel in) {
        codigo = in.readInt();
        codVenda = in.readInt();
        fpgto = in.readInt();
        valorPago = in.readDouble();
        parcelas = in.readInt();
    }

    public static final Creator<Pagamento> CREATOR = new Creator<Pagamento>() {
        @Override
        public Pagamento createFromParcel(Parcel in) {
            return new Pagamento(in);
        }

        @Override
        public Pagamento[] newArray(int size) {
            return new Pagamento[size];
        }
    };

    public static final String getFormaPagamento(int fpgto){
        // METODO PARA RETORNAR O NOME DA FORMA DE PAGAMENTO SOLICITADA

        switch (fpgto){
            case DINHEIRO: return "Dinheiro";
            case CHEQUE: return "Cheque";
            case CARTAO_CREDITO: return "Cartão de Crédito";
            case CARTAO_DEBITO: return "Cartão de Débito";
            case CREDITO_LOJA: return "Crédito Loja";
            case VALE_ALIMENTACAO: return "Vale Alimentação";
            case VALE_REFEICAO: return "Vale Refeição";
            case VALE_PRESENTE: return "Vale Presente";
            case VALE_COMBUSTIVEL: return "Vale Combustível";
            case OUTROS: return "Outros";
            default: return "";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(codigo);
        parcel.writeInt(codVenda);
        parcel.writeInt(fpgto);
        parcel.writeDouble(valorPago);
        parcel.writeInt(parcelas);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
