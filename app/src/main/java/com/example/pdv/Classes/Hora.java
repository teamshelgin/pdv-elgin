package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.Calendar;


/** CLASSE QUE REPRESENTA UM HORARIO **/
public class Hora implements Parcelable {
    private Calendar c = Calendar.getInstance();

    private int hora;
    private int minuto;
    private int segundo;

    public Hora(){
        setHora(c.get(Calendar.HOUR_OF_DAY));
        setMinuto(c.get(Calendar.MINUTE));
        setSegundo(c.get(Calendar.SECOND));
    }

    public int getHora(){return this.hora;}
    public void setHora(int hora) {
        if (hora < 0) hora *= -1;
        this.hora = hora % 24;
    }

    public int getMinuto() { return minuto; }
    public void setMinuto(int minuto) {
        if (minuto < 0) minuto *= -1;
        this.minuto = minuto % 60;
    }

    public int getSegundo() { return segundo; }
    public void setSegundo(int segundo) {
        if (segundo < 0) segundo *= -1;
        this.segundo = segundo % 60;
    }

    public String toString(){
        String hora, minuto, segundo;

        if (getHora() < 10){
            hora = "0" + getHora();
        }else{
            hora = Integer.toString(getHora());
        }

        if (getMinuto() < 10){
            minuto = "0" + getMinuto();
        }else{
            minuto = Integer.toString(getMinuto());
        }

        if (getSegundo() < 10){
            segundo = "0" + getSegundo();
        }else{
            segundo = Integer.toString(getSegundo());
        }

        return hora + ":" + minuto + ":" + segundo;
    }

    public static Hora getHora(String inHora){
        int hora, minuto, segundo;
        Hora horario = new Hora();

        if (inHora == null) inHora = ""; // Previne NullPointerException

        String[] outHora = inHora.split(":");
        if (outHora.length != 3) return horario;

        try{
            hora = Integer.parseInt(outHora[0]);
            minuto = Integer.parseInt(outHora[1]);
            segundo = Integer.parseInt(outHora[2]);
        } catch (NumberFormatException e){
            e.printStackTrace();
            return horario;
        }

        horario.setHora(hora);
        horario.setMinuto(minuto);
        horario.setSegundo(segundo);
        return horario;
    }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Hora(Parcel in) {
        hora = in.readInt();
        minuto = in.readInt();
        segundo = in.readInt();
    }

    public static final Creator<Hora> CREATOR = new Creator<Hora>() {
        @Override
        public Hora createFromParcel(Parcel in) {
            return new Hora(in);
        }

        @Override
        public Hora[] newArray(int size) {
            return new Hora[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(hora);
        parcel.writeInt(minuto);
        parcel.writeInt(segundo);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
