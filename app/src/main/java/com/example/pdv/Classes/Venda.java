package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


/** CLASSE QUE REPRESENTA UMA VENDA **/
public class Venda implements Parcelable {
    public static int NUM_VENDAS; // Numero de vendas efetuadas/pendentes

    private int cod;
    private Data data;
    private Hora hora;
    private double valor;
    private boolean aberta;
    private boolean cancelada;
    private ArrayList<ProdutoVenda> produtos;
    private ArrayList<Pagamento> pagamentos;

    public Venda(int cod, Data data, Hora hora){
        this.cod = cod;
        this.data = data;
        this.hora = hora;
        abrir();
        this.produtos = new ArrayList<>();
        this.pagamentos = new ArrayList<>();
    }

    public int getCod() { return cod; }
    public void setCod(int cod) { this.cod = cod; }

    public Data getData() { return data; }
    public void setData(Data data) { this.data = data; }

    public Hora getHora() { return hora; }
    public void setHora(Hora hora) { this.hora = hora; }

    public double getValor() { return valor; }
    public void setValor(double valor) { this.valor = valor; }

    public boolean isAberta() { return aberta; }
    public void abrir() { if (!isAberta()) this.aberta = true; }
    public void fechar() { if (isAberta()) this.aberta = false; }

    public boolean isCancelada() { return cancelada; }
    public void setCancelada(boolean cancelada) { this.cancelada = cancelada; }

    public ArrayList<ProdutoVenda> getProdutos() { return produtos; }
    public void setProdutos(ArrayList<ProdutoVenda> produtos) { this.produtos = produtos; }

    public ArrayList<Pagamento> getPagamentos() { return pagamentos; }
    public void setPagamentos(ArrayList<Pagamento> pagamentos) { this.pagamentos = pagamentos; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Venda(Parcel in) {
        cod = in.readInt();
        data = in.readParcelable(Data.class.getClassLoader());
        hora = in.readParcelable(Hora.class.getClassLoader());
        valor = in.readDouble();
        aberta = in.readByte() != 0;
        cancelada = in.readByte() != 0;
        produtos = in.createTypedArrayList(ProdutoVenda.CREATOR);
        pagamentos = in.createTypedArrayList(Pagamento.CREATOR);
    }

    public static final Creator<Venda> CREATOR = new Creator<Venda>() {
        @Override
        public Venda createFromParcel(Parcel in) {
            return new Venda(in);
        }

        @Override
        public Venda[] newArray(int size) {
            return new Venda[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(cod);
        parcel.writeParcelable(data, i);
        parcel.writeParcelable(hora, i);
        parcel.writeDouble(valor);
        parcel.writeByte((byte) (aberta ? 1 : 0));
        parcel.writeByte((byte) (cancelada ? 1 : 0));
        parcel.writeTypedList(produtos);
        parcel.writeTypedList(pagamentos);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
