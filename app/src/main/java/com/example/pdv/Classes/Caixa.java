package com.example.pdv.Classes;


import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


/** CLASSE QUE REPRESENTA UM CAIXA **/
public class Caixa implements Parcelable {
    public static int NUM_CAIXAS; // Numero de caixas ja' abertos

    private int cod;
    private Data dataAbertura;
    private Hora horaAbertura;
    private double valorAbertura;
    private Data dataFechamento;
    private Hora horaFechamento;
    private double vendaBruta;
    private double cancelamentos;
    private boolean aberto;
    private ArrayList<Venda> vendas;

    public Caixa(int cod, Data dataAbertura, Hora horaAbertura, double valorAbertura){
        this.cod = cod;
        this.dataAbertura = dataAbertura;
        this.horaAbertura = horaAbertura;
        this.valorAbertura = valorAbertura;
        abrir();
        this.vendas = new ArrayList<>();
    }

    public int getCod() { return cod; }
    public void setCod(int cod) { this.cod = cod; }

    public Data getDataAbertura() { return dataAbertura; }
    public void setDataAbertura(Data dataAbertura) { this.dataAbertura = dataAbertura; }

    public Hora getHoraAbertura() { return horaAbertura; }
    public void setHoraAbertura(Hora horaAbertura) { this.horaAbertura = horaAbertura; }

    public double getValorAbertura() { return valorAbertura; }
    public void setValorAbertura(double valorAbertura) { this.valorAbertura = valorAbertura; }

    public Data getDataFechamento() { return dataFechamento; }
    public void setDataFechamento(Data dataFechamento) { this.dataFechamento = dataFechamento; }

    public Hora getHoraFechamento() { return horaFechamento; }
    public void setHoraFechamento(Hora horaFechamento) { this.horaFechamento = horaFechamento; }

    public double getVendaBruta() { return vendaBruta; }
    public void setVendaBruta(double vendaBruta) { this.vendaBruta = vendaBruta; }

    public double getCancelamentos() { return cancelamentos; }
    public void setCancelamentos(double cancelamentos) { this.cancelamentos = cancelamentos; }

    public boolean isAberto() { return aberto; }
    public void abrir() { if (!isAberto()) this.aberto = true; }
    public void fechar() {
        if (isAberto()){
            setDataFechamento(new Data());
            setHoraFechamento(new Hora());
            this.aberto = false;
        }
    }

    public ArrayList<Venda> getVendas() { return vendas; }
    public void setVendas(ArrayList<Venda> vendas) { this.vendas = vendas; }


    // PARCELABLE
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    private Caixa(Parcel in) {
        cod = in.readInt();
        dataAbertura = in.readParcelable(Data.class.getClassLoader());
        horaAbertura = in.readParcelable(Hora.class.getClassLoader());
        valorAbertura = in.readDouble();
        dataFechamento = in.readParcelable(Data.class.getClassLoader());
        horaFechamento = in.readParcelable(Hora.class.getClassLoader());
        vendaBruta = in.readDouble();
        cancelamentos = in.readDouble();
        aberto = in.readByte() != 0;
        vendas = in.createTypedArrayList(Venda.CREATOR);
    }

    public static final Creator<Caixa> CREATOR = new Creator<Caixa>() {
        @Override
        public Caixa createFromParcel(Parcel in) {
            return new Caixa(in);
        }

        @Override
        public Caixa[] newArray(int size) {
            return new Caixa[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(cod);
        parcel.writeParcelable(dataAbertura, i);
        parcel.writeParcelable(horaAbertura, i);
        parcel.writeDouble(valorAbertura);
        parcel.writeParcelable(dataFechamento, i);
        parcel.writeParcelable(horaFechamento, i);
        parcel.writeDouble(vendaBruta);
        parcel.writeDouble(cancelamentos);
        parcel.writeByte((byte) (aberto ? 1 : 0));
        parcel.writeTypedList(vendas);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////
}
