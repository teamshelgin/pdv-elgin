package com.example.pdv;


import android.app.AlertDialog;
import android.content.Context;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.example.pdv.Classes.Venda;
import com.example.pdv.SQLite.DAO;
import static com.example.pdv.SQLite.PostContract.*;
import static com.example.pdv.Classes.Produto.*;


public final class Utils {
    private static final DAO bd = new DAO();


    public static final boolean validarLogin(Context context, String username, String password){
        // VERIFICA SE username E password FORNECIDOS CONSTAM NO BANCO DE DADOS

        String pass;
        try {
            pass = toHex(getSHA256(password), true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }

        String table, selection;
        String[] columns, selectionArgs;
        String[][] RESULT;

        bd.OPEN(context);

        table = LOGINS.TABLE_NAME;
        columns = new String[]{LOGINS.COLUMN_USER, LOGINS.COLUMN_PASSWD};
        selection = LOGINS.COLUMN_USER + " = ?" +  " AND " + LOGINS.COLUMN_PASSWD + " = ?";
        selectionArgs = new String[]{username, pass};

        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        return RESULT.length == 1;
    }

    public static final byte[] getSHA256(String str) throws NoSuchAlgorithmException {
        // OBTEM SHA-256 DE UMA STRING

        if (str == null) str = ""; // Previne NullPointerException
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(str.getBytes(StandardCharsets.UTF_8));
    }

    public static final String toHex(byte[] array, boolean uppercase){
        // SIMULA O FUNCIONAMENTO DO METODO toHex DO Qt

        if (array == null) array = "".getBytes(); // Previne NullPointerException
        StringBuilder hexString = new StringBuilder();

        if (uppercase) {
            for (byte b : array) { hexString.append(String.format("%02X", 0xFF & b)); }
        }else{
            for (byte b : array) { hexString.append(String.format("%02x", 0xFF & b)); }
        }

        return hexString.toString();
    }

    public static final boolean intBoolean(String str){
        // RECEBE UMA STRING, CONVERTE PARA INT
        // E VALIDA SE E' FALSE (0) OU TRUE (!0)

        int i;

        try{
            i = Integer.parseInt(str);
        } catch (NumberFormatException e){
            e.printStackTrace();
            return false;
        }

        return i != 0;
    }

    public static final AlertDialog makeAlert(Context context, int alerta){
        // CODIGOS DE ALETRA DISPONIVEIS
        //  #1 - Tentar criar venda com caixa fechado
        //  #2 - Tentar criar uma venda com sistema sem produtos cadastrados
        //  #3 - Tentar fechar venda sem adicionar produtos
        //  #4 - Adicionar pagamento com saldo ja' pago
        //  #5 - Adicionar pagamento sem valor
        //  #6 - Fechar venda sem pagar o saldo
        //  #7 - Fechar caixa com vendas pendentes
        //  #8 - Cancelar venda que nao esta' aberta
        //  #9 - Venda cancelada com sucesso
        // #10 - Salvar venda que nao esta' aberta
        // #11 - Tentar ver vendas pendentes em caixa sem vendas pendentes
        // #12 - Tentar ver vendas efetuadas em caixa sem vendas efetuadas
        // #13 - Tentar ver vendas com caixa fechado
        // #14 - Venda a cartão finalizada com sucesso!

        AlertDialog a;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        switch (alerta){
            case 1:
                builder.setTitle("Caixa não está aberto");
                builder.setMessage("É necessário abrir um caixa antes de iniciar uma venda.");

                break;
            case 2:
                builder.setTitle("Nenhum produto cadastrado");
                builder.setMessage("É preciso cadastrar produtos no sistema antes de iniciar uma venda.");

                break;
            case 3:
                builder.setTitle("Nenhum produto adicionado");
                builder.setMessage("Não é possível fechar uma venda sem produto(s).");

                break;
            case 4:
                builder.setTitle("Não há mais saldo");
                builder.setMessage("O valor da venda já está pago.");

                break;
            case 5:
                builder.setTitle("Valor não informado");
                builder.setMessage("Digite o valor do pagamento.");

                break;
            case 6:
                builder.setTitle("Saldo restante não foi pago");
                builder.setMessage("Não é possível fechar a venda sem pagar todo o saldo restante.");

                break;
            case 7:
                builder.setTitle("Vendas pendentes");
                builder.setMessage("Não é possível fechar o caixa enquanto houver vendas pendentes.");

                break;
            case 8:
                builder.setTitle("Venda não está aberta");
                builder.setMessage("Esta venda não foi aberta, não é possível cancelá-la.");

                break;
            case 9:
                builder.setTitle("Venda cancelada");
                builder.setMessage("Venda cancelada com sucesso.");

                break;
            case 10:
                builder.setTitle("Venda não está aberta");
                builder.setMessage("Esta venda não foi aberta, não é possível salvá-la.");

                break;
            case 11:
                builder.setTitle("Não há vendas pendentes");
                builder.setMessage("Este caixa não possui vendas pendentes.");

                break;
            case 12:
                builder.setTitle("Não há vendas efetuadas");
                builder.setMessage("Nenhuma venda foi efetuada neste caixa.");

                break;
            case 13:
                builder.setTitle("Caixa fechado");
                builder.setMessage("Não há vendas.");

                break;
            case 14:
                builder.setTitle("Cartão");
                builder.setMessage("Venda Realizada com sucesso!");

                break;
        }

        a = builder.create();
        return a;
    }

    public static final double arithmetic(int op, int scale, int roundingMode, double prim, double... vals){
        // METODO PARA OPERACOES ARITMETICAS BASICAS SEM PERDA DE PRECISAO
        // METODO NAO TRATA EXCESSOES (PARAMETROS INVALIDOS OU DIVISAO POR ZERO, POR EXEMPLO)

        BigDecimal[] bd = new BigDecimal[vals.length];
        BigDecimal result = BigDecimal.valueOf(prim);

        for (int i=0; i<vals.length; i++){
            bd[i] = BigDecimal.valueOf(vals[i]);
        }

        switch (op){
            case 1:{ // Soma
                for (int i=0; i<vals.length; i++){
                    result = result.add(bd[i]);
                }
                break;
            } case 2:{ // Subtracao
                for (int i=0; i<vals.length; i++){
                    result = result.subtract(bd[i]);
                }
                break;
            } case 3:{ // Multiplicacao
                for (int i=0; i<vals.length; i++){
                    result = result.multiply(bd[i]);
                }
                break;
            } case 4:{ // Divisao
                for (int i=0; i<vals.length; i++){
                    result = result.divide(bd[i], scale, roundingMode);
                }
                break;
            }
        }

        return result.doubleValue();
    }

    public static final void imprimeVenda(Context context, Venda venda){
        ArrayList<String> itens = new ArrayList<>();

        for (int i=0; i<venda.getProdutos().size(); i++){
            itens.add(
                    venda.getProdutos().get(i).getCodProd() + "|" +
                    cod2nom(context, venda.getProdutos().get(i).getCodProd()) + "|" +
                    String.format("%.4f", (float)venda.getProdutos().get(i).getQtde()) + "|" +
                    cod2prv(context, venda.getProdutos().get(i).getCodProd())
            );
        }

        com.elgin.e1.Impressora.Termica.AbreConexaoImpressora(5, "SMARTPOS", "", 0);
        com.elgin.e1.Impressora.Termica.ImprimeXMLSAT(GeraXmlSat(itens), 0);
        com.elgin.e1.Impressora.Termica.FechaConexaoImpressora();
    }

    // FEITO PELO GEORGE --> VERIFICAR
    public static final String GeraXmlSat(ArrayList<String> itens) {
        String xml1 = "<CFe><infCFe Id=\"CFe35170851010502000134590003210940232546989432\" versao=\"0.07\" versaoDadosEnt=\"0.07\" versaoSB=\"010100\"><ide><cUF>35</cUF><cNF>698943</cNF><mod>59</mod><nserieSAT>000321094</nserieSAT><nCFe>023254</nCFe><dEmi>20170816</dEmi><hEmi>160807</hEmi><cDV>2</cDV><tpAmb>1</tpAmb><CNPJ>50245869000174</CNPJ><signAC>EiF1aTwp0Xybc/t2V6B4Q2YMqXV3Fzu8p2xjR54hlNJuhFQM23ijA+KZ4awLi2buvmCH2h32Gx3X+3b6sJ7aU4qMDw/ovSHGPAja2GIPqbs3o6v+BmJWSByVVw5FMhbBxKpbTlefVo+VQu4BguhoL3b1pd9obpapm11CvOhA1qoFvMgdkjPS+JlkmViaJgrLolkifxtXLP3aU34lLipPN8AnPN75F10sgQLDm5gRk22+Xk1zdCcb6SBuwlNR/j0NMn4pdZCUIAYVRrEfy10U2EJ8MmPjoUHZU9DYgk2RtAYZ7kYDO9kNIPjhbOcukJQdXdicxybiE1lKPpQHgrAA6A==</signAC><assinaturaQRCODE>HxdoxP9woeIv00fbzSKuqFI2Q1Luw31Tc22jfBO+FN1GgqKXCZ7Ny7mAbSoTOzYfVvw6WLTKi2P9T5ukCInyiNsDCLpaAgB+ITxkrbuWNcJp7i8bIvrJqj/30Z6RpUIeMpjuAL0h4VQB/Yv5HZmZz3RvZOyhf3vNUXDedi1OYj6D+ByHxvtZ1Smt8WHSt9Jx/lfPTE7YyJQ2yjMPJYX61oRoYadSpcNA87/uBUCew80Eszv+1/LrIFD3nay2mCl7t9EOZye7kuBoBiyeucYBQXGHX2Fxz9HO0fROOF7a9m8vf1w3BedBMm7WBE/Ht8suSs7+FvFKSNCB/n8O9b+wUA==</assinaturaQRCODE><numeroCaixa>109</numeroCaixa></ide><emit><CNPJ>51010502000134</CNPJ><xNome>SUPERMERCADO RONDON LTDA</xNome><xFant>SUPERMERCADOS RONDON</xFant><enderEmit><xLgr>RUA MARCOS TOQUETAO</xLgr><nro>514</nro><xBairro>JUSSARA</xBairro><xMun>ARACATUBA</xMun><CEP>16021345</CEP></enderEmit><IE>177034587110</IE><cRegTrib>3</cRegTrib><cRegTribISSQN>1</cRegTribISSQN><indRatISSQN>N</indRatISSQN></emit><dest/>";
        String det, refact;
        float vCFe = 0;
        float qCom, vProd, vItem;
        for (int x = 0; x < itens.size(); x++) {
            String[] split = itens.get(x).split("[|]");
            int tam = x+1;
            det = "<det nItem=\""+tam+"\">" +
                    "<prod>" +
                    "<cProd>"+split[0]+"</cProd>" +
                    "<cEAN>7897380000217</cEAN>" +
                    "<xProd>"+split[1]+"</xProd>" +
                    "<NCM>04012010</NCM>" +
                    "<CFOP>5405</CFOP>" +
                    "<uCom>un</uCom>" +
                    "<qCom>"+split[2]+"</qCom>" +
                    "<vUnCom>"+split[3]+"</vUnCom>" +
                    "<vProd>"+split[3]+"</vProd>";
            qCom = Float.parseFloat(split[2]);
            vProd = Float.parseFloat(split[3]);
            refact = String.format("%.2f", qCom*vProd).replace(",", ".");
            det = det + "<indRegra>A</indRegra>" +
                    "<vItem>"+refact+"</vItem>" +
                    "<obsFiscoDet xCampoDet=\"Cod. CEST\">" +
                    "<xTextoDet>0000000</xTextoDet>" +
                    "</obsFiscoDet>" +
                    "</prod>" +
                    "<imposto><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS>" +
                    "<PIS><PISNT><CST>06</CST></PISNT></PIS><COFINS><COFINSNT><CST>06</CST>" +
                    "</COFINSNT></COFINS></imposto></det>";
            xml1 = xml1+det;
            vCFe = vCFe + Float.parseFloat(split[3]);
        }
        String xml2 = "<total><ICMSTot><vICMS>0.00</vICMS><vProd>8.34</vProd><vDesc>0.00</vDesc><vPIS>0.04</vPIS><vCOFINS>0.22</vCOFINS><vPISST>0.00</vPISST><vCOFINSST>0.00</vCOFINSST><vOutro>0.00</vOutro></ICMSTot><vCFe>"+String.format("%.2f", vCFe).replace(",", ".")+"</vCFe></total><pgto><MP><cMP>01</cMP><vMP>8.34</vMP></MP><vTroco>0.00</vTroco></pgto><infAdic><infCpl>[TRIBUTOS TOTAIS INCIDENTES (LEI FEDERAL 12.741/2012) - FEDERAL 0,61 ESTADUAL 0,74  ] OjU6NzU3Jzc3Nzo3NHRROXZrclZvO052a1IhVXY5RUVoYTo4NzY3Nzo6IzcnNzA6NjYwNic2Nw==</infCpl><obsFisco xCampo=\"02.03.04.03\"><xTexto>Consulte o QRCode deste extrato atraves do App DeOlhoNaNota</xTexto></obsFisco></infAdic></infCFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"#CFe35170851010502000134590003210940232546989432\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>G7FJPtvtuqixyPYX66je1tQnXuVq0My+4AvC2vnSJTk=</DigestValue></Reference></SignedInfo><SignatureValue>VOYVZjLVd2dmr4OaUfnDNT/Rq5WtTixRFrX+NUlwgxnBF9T/Y51GRBr4hw3UpviJahnMafjLP70ZyD3mNvh9dFNZBWTbXpDwztdZEDd+o8goPwnmHVc9ktE+aUTW08B18tqXtVb5eqKEcKDohoGinclVX4I+cIeaRn2DyUuedUh67GXxmScmMeCATnEPSYXrnfSreCim9lSDUjWBGAydMsPmtj9j1LGgfAqeVLi/fE9rZBLRe1UX7mXTvZGVwCrAcyzPG+MgdBPP/9oo6bSwtlGj5OXrNZHmx4VPC+dC6POvUGf8eaAam18DbWM3tN5qbO82+RFmN1qLckrjiFP6VQ==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIGfDCCBGSgAwIBAgIJARWQjPtlC04cMA0GCSqGSIb3DQEBCwUAMFExNTAzBgNVBAoTLFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGUgU2FvIFBhdWxvMRgwFgYDVQQDEw9BQyBTQVQgU0VGQVogU1AwHhcNMTcwNTE5MTUyNzM1WhcNMjIwNTE5MTUyNzM1WjCBtTESMBAGA1UEBRMJMDAwMzIxMDk0MQswCQYDVQQGEwJCUjESMBAGA1UECBMJU2FvIFBhdWxvMREwDwYDVQQKEwhTRUZBWi1TUDEPMA0GA1UECxMGQUMtU0FUMSgwJgYDVQQLEx9BdXRlbnRpY2FkbyBwb3IgQVIgU0VGQVogU1AgU0FUMTAwLgYDVQQDEydTVVBFUk1FUkNBRE8gUk9ORE9OIExUREE6NTEwMTA1MDIwMDAxMzQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGZ+Slrm+HHZo4lCMD+x4ZvPwbGR3VIQVW86T0ZDNspPb1MZ13hdr7c+1jL2qkrbEnYSweaZW9YlVFG1lXIlHw5oAcIKqQd5ooxy2i8pKDkzDJ0GlL4wsoT1uqHNBpyPVosfJkIjFQoHh1kCegz70yYB6+WDVNXDoIbBdt8zyKLafL9g3jd95IqcZTiDM1xC1wPat21KvVY8u9QHDLrvo6CdaxyYBZ1tp1Csq+XoPRt7Q6dBZkwfdQHUlDuIbX/xEEyYhTRGJKwvMASubWZgY8PLmjCBA3QAL2McwszAPFpresasItEWZCMbRLbraMVaxsaCT64Pql9bnd5cpON8ptAgMBAAGjggHwMIIB7DAOBgNVHQ8BAf8EBAMCBeAwdQYDVR0gBG4wbDBqBgkrBgEEAYHsLQMwXTBbBggrBgEFBQcCARZPaHR0cDovL2Fjc2F0LmltcHJlbnNhb2ZpY2lhbC5jb20uYnIvcmVwb3NpdG9yaW8vZHBjL2Fjc2VmYXpzcC9kcGNfYWNzZWZhenNwLnBkZjBlBgNVHR8EXjBcMFqgWKBWhlRodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9sY3IvYWNzYXRzZWZhenNwL2Fjc2F0c2VmYXpzcGNybC5jcmwwgZQGCCsGAQUFBwEBBIGHMIGEMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbXByZW5zYW9maWNpYWwuY29tLmJyMFIGCCsGAQUFBzAChkZodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvYWNzYXQucDdjMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAkGA1UdEwQCMAAwJAYDVR0RBB0wG6AZBgVgTAEDA6AQBA41MTAxMDUwMjAwMDEzNDAfBgNVHSMEGDAWgBSwhYGzKM12KikkS19YSm9o2aywKjANBgkqhkiG9w0BAQsFAAOCAgEAKMwCoOa6CbzUlmjqqGPX2OfciCYrdSx4RU0TLdR5OZTHlpFnHx1tPe72o+j1QjMqziu/HBMwjktptiZLpnsNkDWRFgT9HSXlhtUaZYXdyKuVayTlCC5tiTxEoK8IqfttHVc8FJHrHwHAvu/oxKH5mTUBohKDNTQFhGw9YQieuXeIsqFC8RiL9cQtPSClVddfAb3SH9tUpJBQKuMN8xWh/22cQkY0jw25Pc8VB+zqvTws/e6hNIgX/1BGUk6w7WALf6Vel1YzKcF2XKkoUgtFN4pIPifoIv9bUUb0qtkszC9s2/wI7Bmolvza5wRQ7E+Py3o7cjbNEmb9s5UHQUMt3pEOBrFLVIDAcUU1NYgBUgJzDKMKmiW+RrxtMpE4JAFZAFrwdZzW/evnhknU1rLWZJ0aduBWbTCeAkOMBeiz8jisO2TLi1v5Ap4eHWUH8fAwjeJv84gIb/ENbkYNWgEfzcUxrHYVHWedilAW6EtSVdt1YC8B/fMRYivbweInA0AbRPjVDUf96TTCJSQsbUzY5tt2p188ExE9neBZ0ENoic/H+/XKkbOBiUYNrl9kJxNa+Z6UmUCmi8PvAi1tdb/V/dC9nhmcx04iaKQZref/QK1q6UBmWGVDy79Dgy1F4bmZaxGpJU5eY5LhfPtTgzpcHmRmL7UJu5Xwf+GX12eWBT0=</X509Certificate></X509Data></KeyInfo></Signature></CFe>";
        return xml1+xml2;
    }
}
