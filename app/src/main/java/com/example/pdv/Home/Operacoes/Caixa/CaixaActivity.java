package com.example.pdv.Home.Operacoes.Caixa;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import java.text.NumberFormat;
import java.util.Locale;

import com.example.pdv.R;
import com.example.pdv.SQLite.DAO;
import com.example.pdv.Classes.Data;
import com.example.pdv.Classes.Hora;
import com.example.pdv.Classes.Caixa;
import static com.example.pdv.Utils.*;
import static com.example.pdv.SQLite.PostContract.*;


public class CaixaActivity extends AppCompatActivity {

    private final DAO bd = new DAO();
    private ContentValues values = new ContentValues();

    private Caixa caixa; // in --> Operacoes Fragment

    private TextView textCaixaValue, textDataAberValue, textHoraAberValue,
            textVendaBrutaValue, textCancelamentosValue, textVendaLiquidaValue;

    private EditText editTextValorAber;
    private Button btnAbreFecha;

    private AlertDialog alerta;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caixa);

        // RECUPERA CAIXA
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        caixa = ib.getParcelable("caixa");

        // BARRA SUPERIOR
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // ASSOCIA ELEMENTOS
        textCaixaValue = findViewById(R.id.textCaixaValue);
        textDataAberValue = findViewById(R.id.textDataAberValue);
        textHoraAberValue = findViewById(R.id.textHoraAberValue);
        textVendaBrutaValue = findViewById(R.id.textVendaBrutaValue);
        textCancelamentosValue = findViewById(R.id.textCancelamentosValue);
        textVendaLiquidaValue = findViewById(R.id.textVendaLiquidaValue);

        editTextValorAber = findViewById(R.id.editTextValorAber);
        btnAbreFecha = findViewById(R.id.btnAbreFecha);

        // MONTA VIEW COM BASE NO STATUS DO CAIXA...
        if (caixa == null){
            viewCaixaFechado();
        }else{
            viewCaixaAberto();
        }

        // LISTENER DO btnAbreFecha
        btnAbreFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (caixa == null){ // CAIXA FECHADO - ABRIR CAIXA...

                    abrirCaixa();

                }else { // CAIXA ABERTO - FECHAR CAIXA...

                    builder = new AlertDialog.Builder(CaixaActivity.this);

                    builder.setTitle("Fechar caixa");
                    builder.setMessage("Deseja fechar o caixa?");

                    builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            fecharCaixa();
                        }
                    });

                    builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Nao faz nada
                        }
                    });

                    // OBTEM CONFIRMACAO DO USUARIO...
                    alerta = builder.create();
                    alerta.show();
                }
            }
        });
    }

    private void viewCaixaFechado(){
        textCaixaValue.setText(getString(R.string.text_fechado));
        textDataAberValue.setText("");
        textHoraAberValue.setText("");
        textVendaBrutaValue.setText("");
        textCancelamentosValue.setText("");
        textVendaLiquidaValue.setText("");

        editTextValorAber.setEnabled(true);
        editTextValorAber.setText("0.00");
        btnAbreFecha.setText(getString(R.string.text_abrircaixa));
    }

    private void viewCaixaAberto(){
        NumberFormat numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

        textCaixaValue.setText("#" + caixa.getCod());
        textDataAberValue.setText(caixa.getDataAbertura().toString());
        textHoraAberValue.setText(caixa.getHoraAbertura().toString());
        textVendaBrutaValue.setText(numFormat.format(caixa.getVendaBruta()));
        textCancelamentosValue.setText(numFormat.format(caixa.getCancelamentos()));
        textVendaLiquidaValue.setText(numFormat.format(
                arithmetic(2, -1, -1, caixa.getVendaBruta(), caixa.getCancelamentos())
        )); // caixa.getVendaBruta() - caixa.getCancelamentos()

        editTextValorAber.setEnabled(false);
        editTextValorAber.setText(numFormat.format(caixa.getValorAbertura()));
        btnAbreFecha.setText(getString(R.string.text_fecharcaixa));
    }

    private void abrirCaixa(){
        // OBTEM INFORMACOES DO NOVO CAIXA
        double valorAbertura;
        Caixa.NUM_CAIXAS++;

        try {
            valorAbertura = Double.parseDouble(editTextValorAber.getText().toString());
        }catch (NumberFormatException e){
            e.printStackTrace();
            valorAbertura = 0;
        }

        // ABRE O CAIXA...
        caixa = new Caixa(Caixa.NUM_CAIXAS, new Data(), new Hora(), valorAbertura);

        // SALVA INFORMACOES NO BANCO DE DADOS...
        values.clear();
        values.put(CAIXAS.COLUMN_COD, caixa.getCod());
        values.put(CAIXAS.COLUMN_DATABER, caixa.getDataAbertura().toString());
        values.put(CAIXAS.COLUMN_HORABER, caixa.getHoraAbertura().toString());
        values.put(CAIXAS.COLUMN_VALORABER, caixa.getValorAbertura());
        values.put(CAIXAS.COLUMN_ABERTO, caixa.isAberto());

        bd.OPEN(getApplicationContext());
        bd.INSERT(CAIXAS.TABLE_NAME, null, values);
        bd.CLOSE();

        viewCaixaAberto();
    }

    private void fecharCaixa(){
        int flag = 0;

        // VERIFICA SE HA' VENDAS PENDENTES...
        for (int i=0; i<caixa.getVendas().size(); i++){
            if (caixa.getVendas().get(i).isAberta() && !caixa.getVendas().get(i).isCancelada()){
                flag = 1;
                break;
            }
        }

        if (flag == 0) {
            // FECHA O CAIXA...
            caixa.fechar();

            // SALVA INFORMACOES NO BANCO DE DADOS...
            values.clear();
            values.put(CAIXAS.COLUMN_DATAFECH, caixa.getDataFechamento().toString());
            values.put(CAIXAS.COLUMN_HORAFECH, caixa.getHoraFechamento().toString());
            values.put(CAIXAS.COLUMN_VENDABRUTA, caixa.getVendaBruta());
            values.put(CAIXAS.COLUMN_CANCELAMENTOS, caixa.getCancelamentos());
            values.put(CAIXAS.COLUMN_ABERTO, caixa.isAberto());
            // CAMPOS COD, DATABER, HORABER, VALORABER --> ABERTURA DO CAIXA
            // CAMPOS VENDABRUTA, CANCELAMENTOS --> FECHAMENTO/CANCELAMENTO DE VENDAS
            // CAMPOS DATAFECH, HORAFECH, ABERTO --> FECHAMENTO DO CAIXA

            bd.OPEN(getApplicationContext());
            bd.UPDATE(CAIXAS.TABLE_NAME, values, CAIXAS.COLUMN_COD + " = ?", new String[]{Integer.toString(caixa.getCod())});
            bd.CLOSE();

            caixa = null;
            viewCaixaFechado();
        }else{
            // AVISA QUE HA' VENDAS PENDENTES...
            alerta = makeAlert(CaixaActivity.this, 7);
            alerta.show();
        }
    }
}
