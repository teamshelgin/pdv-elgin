package com.example.pdv.Home.CRUD;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.pdv.R;
import com.example.pdv.SQLite.DAO;
import com.example.pdv.SQLite.PostContract;


public class CRUDFragment extends Fragment {
    private static final DAO bd = new DAO();

    // VARIAVEIS PARA MANIPULACAO DOS DADOS/QUERIES
    private static String table;
    private static String[] columns;
    private static String[][] RESULT;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private TextView textViewTitulo;
    private TextView textViewDados;
    private Button btn1, btn2, btn3, btn4, btn5, btnDelete;

    public CRUDFragment() {
        // Required empty public constructor
    }

    public static CRUDFragment newInstance(String param1, String param2) {
        CRUDFragment fragment = new CRUDFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_crud, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // ASSOCIA ELEMENTOS
        textViewTitulo = view.findViewById(R.id.textViewTitulo);
        textViewDados = view.findViewById(R.id.textViewDadosPag);

        btn1 = view.findViewById(R.id.btn1);
        btn2 = view.findViewById(R.id.btn2);
        btn3 = view.findViewById(R.id.btn3);
        btn4 = view.findViewById(R.id.btn4);
        btn5 = view.findViewById(R.id.btn5);
        btnDelete = view.findViewById(R.id.btnDelete);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = "";

                table = PostContract.PAGAMENTOS.TABLE_NAME;
                columns = new String[]{
                        PostContract.PAGAMENTOS._ID,
                        PostContract.PAGAMENTOS.COLUMN_CODVENDA,
                        PostContract.PAGAMENTOS.COLUMN_FPGTO,
                        PostContract.PAGAMENTOS.COLUMN_VALOR,
                        PostContract.PAGAMENTOS.COLUMN_PARCELAS
                };

                bd.OPEN(getContext());
                RESULT = bd.SELECT(table, columns, null, null, null, null, null);
                bd.CLOSE();

                content += "[ID] [CODVENDA] [FPGTO] [VALOR] [nPARCELAS]\n";
                for (int i=0; i<RESULT.length; i++){
                    content +=
                            RESULT[i][0] + " " + RESULT[i][1] + "     " + RESULT[i][2] + "     " + RESULT[i][3] + "     " + RESULT[i][4] + "\n";
                }

                textViewTitulo.setText("Tabela Pagamentos");
                textViewDados.setText(content);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = "";

                table = PostContract.PRODUTOS_VENDAS.TABLE_NAME;
                columns = new String[]{
                        PostContract.PRODUTOS_VENDAS._ID,
                        PostContract.PRODUTOS_VENDAS.COLUMN_CODPROD,
                        PostContract.PRODUTOS_VENDAS.COLUMN_CODVENDA,
                        PostContract.PRODUTOS_VENDAS.COLUMN_QTDE,
                        PostContract.PRODUTOS_VENDAS.COLUMN_DESC
                };

                bd.OPEN(getContext());
                RESULT = bd.SELECT(table, columns, null, null, null, null, null);
                bd.CLOSE();

                content += "[ID] [CODPROD] [CODVENDA] [QTDE] [DESC]\n";
                for (int i=0; i<RESULT.length; i++){
                    content +=
                            RESULT[i][0] + " " + RESULT[i][1] + "     " + RESULT[i][2] + "     " + RESULT[i][3] + "     " + RESULT[i][4] + "\n";
                }

                textViewTitulo.setText("Tabela ProdutosVendas");
                textViewDados.setText(content);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = "";

                table = PostContract.VENDAS.TABLE_NAME;
                columns = new String[]{
                        PostContract.VENDAS.COLUMN_COD,
                        PostContract.VENDAS.COLUMN_DATA,
                        PostContract.VENDAS.COLUMN_HORA,
                        PostContract.VENDAS.COLUMN_VALOR,
                        PostContract.VENDAS.COLUMN_ABERTA,
                        PostContract.VENDAS.COLUMN_CANCELADA
                };

                bd.OPEN(getContext());
                RESULT = bd.SELECT(table, columns, null, null, null, null, null);
                bd.CLOSE();

                content += "[COD] [DATA] [HORA] [VALOR] [ABERTA] [CANCELADA]\n";
                for (int i=0; i<RESULT.length; i++){
                    content +=
                            RESULT[i][0] + "     " + RESULT[i][1] + "     " + RESULT[i][2] + "     " +
                                    RESULT[i][3] + "     " + RESULT[i][4] + "     " + RESULT[i][5] + "\n";
                }

                textViewTitulo.setText("Tabela Vendas");
                textViewDados.setText(content);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = "";

                table = PostContract.VENDAS_CAIXAS.TABLE_NAME;
                columns = new String[]{
                        PostContract.VENDAS_CAIXAS.COLUMN_CODVENDA,
                        PostContract.VENDAS_CAIXAS.COLUMN_CODCAIXA
                };

                bd.OPEN(getContext());
                RESULT = bd.SELECT(table, columns, null, null, null, null, null);
                bd.CLOSE();

                content += "[CODVENDA] [CODCAIXA]\n";
                for (int i=0; i<RESULT.length; i++){
                    content +=
                            RESULT[i][0] + "     " + RESULT[i][1] + "\n";
                }

                textViewTitulo.setText("Tabela VendasCaixas");
                textViewDados.setText(content);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = "";

                table = PostContract.CAIXAS.TABLE_NAME;
                columns = new String[]{
                        PostContract.CAIXAS.COLUMN_COD,
                        PostContract.CAIXAS.COLUMN_DATABER,
                        PostContract.CAIXAS.COLUMN_HORABER,
                        PostContract.CAIXAS.COLUMN_VALORABER,
                        PostContract.CAIXAS.COLUMN_DATAFECH,
                        PostContract.CAIXAS.COLUMN_HORAFECH,
                        PostContract.CAIXAS.COLUMN_VENDABRUTA,
                        PostContract.CAIXAS.COLUMN_CANCELAMENTOS,
                        PostContract.CAIXAS.COLUMN_ABERTO
                };

                bd.OPEN(getContext());
                RESULT = bd.SELECT(table, columns, null, null, null, null, null);
                bd.CLOSE();

                content += "[COD] [DATABER] [HORABER] [VALORABER] [DATAFECH] [HORAFECH] [VENDABRUTA] [CANCELAMENTOS] [ABERTO]\n";
                for (int i=0; i<RESULT.length; i++){
                    content +=
                            RESULT[i][0] + "     " + RESULT[i][1] + "     " + RESULT[i][2] + "     " +
                                    RESULT[i][3] + "     " + RESULT[i][4] + "     " + RESULT[i][5] + "     " +
                                    RESULT[i][6] + "     " + RESULT[i][7] + "     " + RESULT[i][8] + "\n";
                }

                textViewTitulo.setText("Tabela Caixas");
                textViewDados.setText(content);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bd.OPEN(getContext());
                bd.DELETE(PostContract.PRODUTOS_VENDAS.TABLE_NAME, null, null);
                bd.DELETE(PostContract.VENDAS_CAIXAS.TABLE_NAME, null, null);
                bd.DELETE(PostContract.PAGAMENTOS.TABLE_NAME, null, null);
                bd.DELETE(PostContract.VENDAS.TABLE_NAME, null, null);
                bd.DELETE(PostContract.CAIXAS.TABLE_NAME, null, null);

                bd.CLOSE();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
