package com.example.pdv.Home.Operacoes.PDV;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pdv.Classes.ElginPAY;
import com.example.pdv.Classes.Pagamento;
import com.example.pdv.Classes.Venda;
import com.example.pdv.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import br.com.setis.interfaceautomacao.Cartoes;
import br.com.setis.interfaceautomacao.EntradaTransacao;
import br.com.setis.interfaceautomacao.Financiamentos;
import br.com.setis.interfaceautomacao.Operacoes;
import br.com.setis.interfaceautomacao.SaidaTransacao;

import static com.example.pdv.Classes.Pagamento.DINHEIRO;
import static com.example.pdv.Utils.arithmetic;
import static com.example.pdv.Utils.makeAlert;


public class PagamentoActivity extends AppCompatActivity {

    private static final String ARG_VENDA = "venda";

    private Venda venda; // in/out --> PDV Activity
    private ArrayList<Pagamento> pagamentos;

    private TextView textViewTotValue, textViewSaldoValue, textViewSaldo;
    private EditText editText;
    private Button btnAddSaldo, btn_forma_pagamento, btnFechar;

    //Implementado por Bruno Cruz
    private Button btnPagamentoCredito, btnPagamentoDebito, btnPagamentoDinheiro, btnCancelar;
    private LinearLayout fundo_sombreado;
    private GridLayout menu_forma_pagamento;
    private Animation move_up, move_down;

    private EntradaTransacao entradaTransacao;
    private Handler handler;
    private ElginPAY pay;
    private static Message message;
    //===============

    private ListView listView;
    private ArrayList<String> list;
    private CustomArrayAdapterPagamento adapter;

    private Bundle ob;
    private NumberFormat numFormat;
    private AlertDialog alerta;
    private AlertDialog.Builder builder;

    private double totalPagar;
    private static double saldoTroco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento);

        // RECUPERA VENDA
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        venda = ib.getParcelable("venda");

        // VARIAVEIS DE AMBIENTE
        totalPagar = venda.getValor();
        saldoTroco = totalPagar;
//        addPgto = 0;

        // ASSOCIA ELEMENTOS

        btnPagamentoCredito = findViewById(R.id.btnPagamentoCredito);
        btnPagamentoDebito = findViewById(R.id.btnPagamentoDebito);
        btnPagamentoDinheiro = findViewById(R.id.btnPagamentoDinheiro);
        btnCancelar = findViewById(R.id.btnCancelar);

        fundo_sombreado = findViewById(R.id.fundo_sombreado);
        menu_forma_pagamento = findViewById(R.id.menu_forma_pagamento);

        move_up = AnimationUtils.loadAnimation(this, R.anim.move_up);
        move_down = AnimationUtils.loadAnimation(this, R.anim.move_down);

        textViewTotValue = findViewById(R.id.textViewTotValue);
        textViewSaldoValue = findViewById(R.id.textViewSaldoValue);
        textViewSaldo = findViewById(R.id.textViewSaldo);

        editText = findViewById(R.id.editText);

        btnAddSaldo = findViewById(R.id.btnAddSaldo);
        btn_forma_pagamento = findViewById(R.id.btn_forma_pagamento);
        btnFechar = findViewById(R.id.btnFechar);

        listView = findViewById(R.id.listview);
        list = new ArrayList<>();

        adapter = new CustomArrayAdapterPagamento(list, this);
        adapter.setTextViewSaldo(textViewSaldo);
        adapter.setTextViewSaldoValue(textViewSaldoValue);
        listView.setAdapter(adapter);

        // INFORMA VALORES
        numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

        textViewTotValue.setText(numFormat.format(totalPagar));
        textViewSaldoValue.setText(numFormat.format(saldoTroco));

        // MANIPULA BOTOES
        btnAddSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(Double.toString(saldoTroco));
            }
        });

        //IMPLEMENTAÇÃO BRUNO CRUZ
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minimizaMenuPagamento();
            }
        });

        fundo_sombreado.setVisibility(View.GONE);
        menu_forma_pagamento.setVisibility(View.GONE);
        fundo_sombreado.setAlpha(0);
        menu_forma_pagamento.setAlpha(0);

        btn_forma_pagamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //IMPLEMENTAÇÃO BRUNO CRUZ
                ((InputMethodManager) getApplicationContext().getSystemService(getApplicationContext().INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                        editText.getWindowToken(), 0);

                fundo_sombreado.setVisibility(View.VISIBLE);
                menu_forma_pagamento.startAnimation(move_up);
                fundo_sombreado.setAlpha(1);

                menu_forma_pagamento.setVisibility(View.VISIBLE);
                menu_forma_pagamento.startAnimation(move_up);
                menu_forma_pagamento.setAlpha(1);
                setEnableComponentes(false);
                return;

            }

        });

        btnFechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (saldoTroco > 0){
                    alerta = makeAlert(PagamentoActivity.this, 6);
                    alerta.show();
                }else{
                    builder = new AlertDialog.Builder(PagamentoActivity.this);

                    builder.setTitle("Fechar venda");
                    builder.setMessage("Deseja confirmar o pagamento e fechar a venda?");

                    builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            fecharPagamento();
                        }
                    });

                    builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            // Nao faz nada
                        }
                    });

                    // OBTEM CONFIRMACAO DO USUARIO...
                    alerta = builder.create();
                    alerta.show();
                }
            }
        });


        btnPagamentoCredito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validaPagamento()) {
                    Random r  = new Random();

                    entradaTransacao = new EntradaTransacao(Operacoes.VENDA,
                            String.valueOf(r.nextInt(100)));


                    entradaTransacao.informaValorTotal(obtemValorAPagarToString());
                    entradaTransacao.informaTipoCartao(Cartoes.CARTAO_CREDITO);
//                    entradaTransacao.informaTipoFinanciamento(Financiamentos.A_VISTA);

                    pay = new ElginPAY(entradaTransacao, handler, PagamentoActivity.this);
                    pay.start();
                }
            }
        });

        btnPagamentoDebito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validaPagamento()) {
                    Random r  = new Random();

                    entradaTransacao = new EntradaTransacao(Operacoes.VENDA,
                            String.valueOf(r.nextInt(100)));

                    entradaTransacao.informaValorTotal(obtemValorAPagarToString());
                    entradaTransacao.informaTipoCartao(Cartoes.CARTAO_DEBITO);

                    pay = new ElginPAY(entradaTransacao, handler, PagamentoActivity.this);
                    pay.start();

                }
            }
        });

        btnPagamentoDinheiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validaPagamento())
                    finalizaPagamento(DINHEIRO);
            }
        });

        /**
         * @AUTHOR: BRUNO CRUZ
         * @DATA: 23/03/2020

            HANDLE SERA ENVIADO PARA CLASSE ElginPAY E FICARA RESPOSAVEL POR PROCESSAR O RETORNO
            O RETORNO SERA PROCESSADO NO METODO HANDLEMESSAGE QUANDO A TRHEAD DE PROCESSAMENTO
            INVOCAR O METODO SENDMESSAGE
            REF: https://developer.android.com/training/multiple-threads/communicate-ui

         */
        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(@NonNull final Message msg) {
                SaidaTransacao s = (SaidaTransacao) msg.obj;
                finalizaPagamento(s.obtemTipoCartao().obtemTipoCartao());
            }
        };
    }

    @Override
    public void onBackPressed() {
        builder = new AlertDialog.Builder(PagamentoActivity.this);

        builder.setTitle("Cancelar pagamento");
        builder.setMessage("Deseja cancelar o pagamento?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                PagamentoActivity.super.onBackPressed();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                // Nao faz nada
            }
        });

        // OBTEM CONFIRMACAO DO USUARIO...
        alerta = builder.create();
        alerta.show();
    }

    private void fecharPagamento(){
        pagamentos = new ArrayList<>();

        /* TODO
            No futuro, analisar o meio de pagamento utilizado (dados[0]),
            bem como o numero de parcelas, caso Cartao de Credito */

        String[] dados;
        for (int i=0; i<list.size(); i++){
            dados = list.get(i).split("\\|");
            pagamentos.add(new Pagamento(venda.getCod(), DINHEIRO, Double.parseDouble(dados[1])));
        }

        venda.setPagamentos(pagamentos);
        pagamentos = null;

        ob = new Bundle();
        ob.putParcelable(ARG_VENDA, venda);

        Intent in = new Intent();
        in.putExtras(ob);

        setResult(1, in);
        finish();
    }

    private boolean validaPagamento(){

        double addPgto = obtemValorAPagar();

        if (saldoTroco <= 0){
            alerta = makeAlert(PagamentoActivity.this, 4);
            alerta.show();
            return false;
        } else if (addPgto <= 0){
            alerta = makeAlert(PagamentoActivity.this, 5);
            alerta.show();
            return false;
        } else{
            return true;
        }
    }

    private void finalizaPagamento(int formaPagamento){

        saldoTroco = arithmetic(2, -1, -1, saldoTroco, obtemValorAPagar()); // saldoTroco - addPgto

        if (saldoTroco < 0) {
            textViewSaldo.setText(R.string.text_troco);
            textViewSaldoValue.setText(numFormat.format(saldoTroco*-1));
        }else{
            textViewSaldo.setText(R.string.text_saldo);
            textViewSaldoValue.setText(numFormat.format(saldoTroco));
        }

        list.add(Pagamento.getFormaPagamento(formaPagamento) + "|" + obtemValorAPagar());
        listView.setAdapter(adapter);

        minimizaMenuPagamento();
    }

    private void setEnableComponentes(boolean value){
        btnFechar.setEnabled(value);
        btnAddSaldo.setEnabled(value);
        btn_forma_pagamento.setEnabled(value);
        editText.setEnabled(value);
    }

    private void minimizaMenuPagamento(){
        menu_forma_pagamento.startAnimation(move_down);
        menu_forma_pagamento.setVisibility(View.GONE);
//                menu_forma_pagamento.setAlpha(0);
        fundo_sombreado.setVisibility(View.GONE);
//                fundo_sombreado.setAlpha(0);
        setEnableComponentes(true);
    }

    private double obtemValorAPagar(){
//      CAPTURA O VALOR PARA REALIZAR O PAGAMENTO
        double addPgto = 0;
        try {
            addPgto = Double.parseDouble(editText.getText().toString());
        }catch (NumberFormatException e){
            e.printStackTrace();
            addPgto = 0;
        }
        return addPgto;
    }

    private String obtemValorAPagarToString(){
        String valor = String.format("%.2f", obtemValorAPagar());
        valor = valor.replace(".","");
        valor = valor.replace(",","");
        return valor;
    }

    // GETTER E SETTER DE saldoTroco, ACESSADOS EM CustomArrayAdapterPagamento
    public static double getSaldoTroco() { return saldoTroco; }
    public static void setSaldoTroco(double s) { saldoTroco = s; }

}
