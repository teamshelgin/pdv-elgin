package com.example.pdv.Home;


import android.net.Uri;
import android.os.Bundle;

import com.example.pdv.Home.CRUD.CRUDFragment;
import com.example.pdv.Home.Configuracoes.ConfiguracoesFragment;
import com.example.pdv.Home.Dashboard.DashboardFragment;
import com.example.pdv.Home.Operacoes.OperacoesFragment;
import com.example.pdv.Home.Relatorios.RelatoriosFragment;
import com.example.pdv.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.MenuItem;
import static com.example.pdv.GetClasses.*;


public class HomeActivity extends AppCompatActivity
    implements CRUDFragment.OnFragmentInteractionListener,
                OperacoesFragment.OnFragmentInteractionListener,
                DashboardFragment.OnFragmentInteractionListener,
                RelatoriosFragment.OnFragmentInteractionListener,
                ConfiguracoesFragment.OnFragmentInteractionListener {

    private ActionBar toolbar;

    // LISTENER DO navView
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_crud:
                    if (toolbar != null) toolbar.setTitle(R.string.title_crud);
                    openFragment(CRUDFragment.newInstance("", ""));

                    return true;
                case R.id.navigation_operacoes:
                    if (toolbar != null) toolbar.setTitle(R.string.title_operacoes);
                    openFragment(OperacoesFragment.newInstance());

                    return true;
                case R.id.navigation_dashboard:
                    if (toolbar != null) toolbar.setTitle(R.string.title_dashboard);
                    openFragment(DashboardFragment.newInstance("", ""));

                    return true;
                case R.id.navigation_relatorios:
                    if (toolbar != null) toolbar.setTitle(R.string.title_relatorios);
                    openFragment(RelatoriosFragment.newInstance("", ""));

                    return true;
                case R.id.navigation_configuracoes:
                    if (toolbar != null) toolbar.setTitle(R.string.title_configuracoes);
                    openFragment(ConfiguracoesFragment.newInstance("", ""));

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = getSupportActionBar();

        // EXIBE MENU INFERIOR
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.navigation_dashboard); // Pre-seleciona Dashboard (CORRIGIR!)

        // PREPARA AMBIENTE
        setAmbiente(getApplicationContext());
    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // TODO
    }
}
