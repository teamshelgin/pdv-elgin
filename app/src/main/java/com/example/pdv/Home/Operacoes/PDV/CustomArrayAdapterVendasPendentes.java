package com.example.pdv.Home.Operacoes.PDV;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pdv.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class CustomArrayAdapterVendasPendentes extends BaseAdapter implements ListAdapter {

    private ArrayList<String> list;
    private Context context;
    private NumberFormat numFormat;

    private String[] dados;

    CustomArrayAdapterVendasPendentes(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
        this.numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_vendas_pendentes, null);
        }

        // MANIPULA TEXTVIEW DA LISTVIEW
        TextView listItemCodVP = view.findViewById(R.id.list_item_cod_vp);
        TextView listItemDataberVP = view.findViewById(R.id.list_item_databer_vp);
        TextView listItemHoraberVP = view.findViewById(R.id.list_item_horaber_vp);
        TextView listItemValorVP = view.findViewById(R.id.list_item_valor_vp);
        dados = list.get(position).split("\\|");

        listItemCodVP.setText(dados[0]);
        listItemDataberVP.setText(dados[1]);
        listItemHoraberVP.setText(dados[2]);
        listItemValorVP.setText(numFormat.format(Double.parseDouble(dados[3])));

        return view;
    }
}
