package com.example.pdv.Home.Operacoes.PDV;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.example.pdv.R;
import com.example.pdv.Classes.Grupo;
import com.example.pdv.Classes.Produto;
import static com.example.pdv.Classes.Produto.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class AddProdutoActivity extends AppCompatActivity {

    private static final String ARG_PRODUTO = "produto";

    private ArrayList<Grupo> grupos; // in --> PDV Activity
    private ArrayList<Produto> produtos; // in --> PDV Activity
    private int codProduto; // out --> PDV Activity

    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;

    private Bundle ob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_produto);

        // RECUPERA GRUPOS E PRODUTOS
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        grupos = ib.getParcelableArrayList("grupos");
        produtos = ib.getParcelableArrayList("produtos");

        // ASSOCIA ELEMENTOS
        expandableListView = findViewById(R.id.expandableListView);
        expandableListDetail = getData();
        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);

        // GROUP EXPAND LISTENER
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {}
        });

        // GROUP COLLAPSE LISTENER
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {}
        });

        // CHILD CLICK LISTENER
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                codProduto = nom2cod(getApplicationContext(),
                        expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition));

                ob = new Bundle();
                ob.putInt(ARG_PRODUTO, codProduto);

                Intent in = new Intent();
                in.putExtras(ob);

                setResult(1, in);
                finish();

                return false;
            }
        });
    }

    // RETORNA HASHMAP DOS GRUPOS/PRODUTOS CADASTRADOS
    public LinkedHashMap<String, List<String>> getData() {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<>();

        int codGrp;
        String nomGrp;
        List<String> prods;

        for (int i=0; i<grupos.size(); i++){
            codGrp = grupos.get(i).getCodigo();
            nomGrp = grupos.get(i).getNome().toUpperCase();

            prods = new ArrayList<>();
            for (int j=0; j<produtos.size(); j++) {
                if (produtos.get(j).getGrupo() == codGrp) {
                    prods.add(produtos.get(j).getNome().toUpperCase());
                }
            }

            expandableListDetail.put(nomGrp, prods);
        }

        prods = null;
        return expandableListDetail;
    }
}
