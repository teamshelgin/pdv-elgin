package com.example.pdv.Home.Operacoes.PDV;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.pdv.R;
import com.example.pdv.Classes.Venda;

import java.util.ArrayList;


public class VendasPendentesActivity extends AppCompatActivity {

    private static final String ARG_COD_VENDA = "codVenda";

    private ArrayList<Venda> vendasPendentes; // in --> PDV Activity
    private int codVenda; // out --> PDV Activity

    private ListView listView;
    private ArrayList<String> list;
    private CustomArrayAdapterVendasPendentes adapter;

    private Bundle ob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendas_pendentes);

        // RECUPERA VENDAS PENDENTES
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        vendasPendentes = ib.getParcelableArrayList("vendasPendentes");

        // MONTA LIST VIEW COM AS VENDAS PENDENTES...
        listView = findViewById(R.id.listview);
        list = new ArrayList<>();

        for (int i=0; i<vendasPendentes.size(); i++){
            list.add(vendasPendentes.get(i).getCod() + "|" +
                    vendasPendentes.get(i).getData().toString() + "|" +
                    vendasPendentes.get(i).getHora().toString() + "|" +
                    vendasPendentes.get(i).getValor()
            );
        }

        adapter = new CustomArrayAdapterVendasPendentes(list, this);
        listView.setAdapter(adapter);

        // LISTENER DO listView (RETORNAR CODIGO DA VENDA PENDENTE SELECIONADA)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                codVenda = vendasPendentes.get(i).getCod();

                ob = new Bundle();
                ob.putInt(ARG_COD_VENDA, codVenda);

                Intent in = new Intent();
                in.putExtras(ob);

                setResult(1, in);
                finish();
            }
        });
    }
}
