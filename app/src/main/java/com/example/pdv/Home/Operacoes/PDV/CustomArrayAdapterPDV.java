package com.example.pdv.Home.Operacoes.PDV;


import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pdv.R;
import com.example.pdv.Classes.Venda;
import com.example.pdv.Classes.ProdutoVenda;
import com.example.pdv.SQLite.DAO;

import static com.example.pdv.Utils.*;
import static com.example.pdv.SQLite.PostContract.*;
import static com.example.pdv.Home.Operacoes.PDV.PDVActivity.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class CustomArrayAdapterPDV extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list;
    private Context context;
    private NumberFormat numFormat;

    private TextView textViewNumItensValue, textViewValTotValue;
    private String[] dados;

    private Venda venda;
    private ArrayList<ProdutoVenda> aprodv;
    private int deletedId;

    private final DAO bd = new DAO();
    private ContentValues values = new ContentValues();

    CustomArrayAdapterPDV(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
        this.numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_pdv, null);
        }

        // MANIPULA TEXTVIEW DA LISTVIEW
        TextView listItemQtde = view.findViewById(R.id.list_item_qtde);
        TextView listItemNome = view.findViewById(R.id.list_item_nome);
        TextView listItemValor = view.findViewById(R.id.list_item_valor);
        TextView listItemDesconto = view.findViewById(R.id.list_item_desconto);
        TextView listItemTotal = view.findViewById(R.id.list_item_total);
        dados = list.get(position).split("\\|");

        listItemQtde.setText(dados[0]);
        listItemNome.setText(dados[1]);
        listItemValor.setText(dados[2]);
        listItemDesconto.setText(dados[3]);
        listItemTotal.setText(dados[4]);

        // MANIPULA BOTAO REMOVER
        ImageView deleteBtn = view.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dados = list.get(position).split("\\|");
                list.remove(position); // (*)

                // (*) Nao e' util, pois o cupom e' "zerado" e remontado com base
                // nos (novos) dados do banco de dados, que foram modificados aqui

                // OBTEM DADOS...
                venda = getVenda();
                aprodv = venda.getProdutos();
                deletedId = aprodv.get(position).getCodigo();

                // REMOVE PRODUTO E ATUALIZA PRECO DA VENDA...
                aprodv.remove(position);
                venda.setValor(
                        arithmetic(2, -1, -1, venda.getValor(), Double.parseDouble(dados[4]))
                ); // venda.getValor() - total

                venda.setProdutos(aprodv);
                setVenda(venda);

                // SALVA INFORMACOES NO BANCO DE DADOS...
                ////////////////////////////////////////////////////////////////////////////////////////////////////
                // PRODUTOS_VENDA
                bd.OPEN(v.getContext());
                bd.DELETE(PRODUTOS_VENDAS.TABLE_NAME, PRODUTOS_VENDAS._ID + " = ?", new String[]{Integer.toString(deletedId)});
                bd.CLOSE();

                // VENDA
                values.clear();
                values.put(VENDAS.COLUMN_VALOR, venda.getValor());

                bd.OPEN(v.getContext());
                bd.UPDATE(VENDAS.TABLE_NAME, values, VENDAS.COLUMN_COD + " = ?", new String[]{Integer.toString(venda.getCod())});
                bd.CLOSE();
                ////////////////////////////////////////////////////////////////////////////////////////////////////

                // ATUALIZA RESUMO DA VENDA...
                int numItens = Integer.parseInt(textViewNumItensValue.getText().toString()) - Integer.parseInt(dados[0]);
                textViewNumItensValue.setText(Integer.toString(numItens));
                textViewValTotValue.setText(numFormat.format(venda.getValor()));

                notifyDataSetChanged();
            }
        });

        return view;
    }

    void setTextViewNumItensValue(TextView textViewNumItensValue) { this.textViewNumItensValue = textViewNumItensValue; }
    void setTextViewValTotValue(TextView textViewValTotValue) { this.textViewValTotValue = textViewValTotValue; }
}
