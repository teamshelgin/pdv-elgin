package com.example.pdv.Home.Operacoes;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pdv.Classes.Caixa;
import com.example.pdv.Classes.ElginPAY;
import com.example.pdv.Home.Operacoes.Caixa.CaixaActivity;
import com.example.pdv.Home.Operacoes.PDV.PDVActivity;
import com.example.pdv.Home.Operacoes.PDV.PagamentoActivity;
import com.example.pdv.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import br.com.setis.interfaceautomacao.EntradaTransacao;
import br.com.setis.interfaceautomacao.Operacoes;
import br.com.setis.interfaceautomacao.SaidaTransacao;

import static com.example.pdv.GetClasses.getCaixa;


public class OperacoesFragment extends Fragment {

    private static final String ARG_CAIXA = "caixa";

    private Caixa caixa; // out --> Caixa Activity, PDV Activity

    private OnFragmentInteractionListener mListener;

    private ImageButton btnPDV, btnCaixa;
    //OPERAÇÕES ELGINPAY
    private ImageButton btnAdm, btnCancelarVendas;//, btnReimpressao,btnConsultar;
    private Intent intentPDV, intentCaixa;
    private Bundle ob;

    private EntradaTransacao entradaTransacao;
    private Handler handler;
    private ElginPAY pay;


    public OperacoesFragment() {
        // Required empty public constructor
    }

    public static OperacoesFragment newInstance() {
        return new OperacoesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // INTENTS
        intentPDV = new Intent(getContext(), PDVActivity.class);
        intentCaixa = new Intent(getContext(), CaixaActivity.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operacoes, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // ASSOCIA ELEMENTOS
        btnPDV = view.findViewById(R.id.btnPDV);
        btnCaixa = view.findViewById(R.id.btnCaixa);

        //ELGINPAY
        btnAdm = view.findViewById(R.id.btnAdm);
        btnCancelarVendas = view.findViewById(R.id.btnCancelarVendas);
        //btnReimpressao = view.findViewById(R.id.btnReimpressao);
        //btnConsultar = view.findViewById(R.id.btnConsultar);

        btnPDV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ob = new Bundle();
                ob.putParcelable(ARG_CAIXA, caixa);

                intentPDV.putExtras(ob);
                startActivity(intentPDV);
            }
        });

        btnCaixa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ob = new Bundle();
                ob.putParcelable(ARG_CAIXA, caixa);

                intentCaixa.putExtras(ob);
                startActivity(intentCaixa);
            }
        });

        /**
         * @AUTHOR: BRUNO CRUZ
         * @DATA: 23/03/2020

            HANDLE SERA ENVIADO PARA CLASSE ElginPAY E FICARA RESPOSAVEL POR PROCESSAR O RETORNO
            O RETORNO SERA PROCESSADO NO METODO HANDLEMESSAGE QUANDO A TRHEAD DE PROCESSAMENTO
            INVOCAR O METODO SENDMESSAGE
            REF: https://developer.android.com/training/multiple-threads/communicate-ui

         */

        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(@NonNull final Message msg) {
                Toast.makeText(getContext(), "Fim da operação", Toast.LENGTH_LONG).show();
            }
        };

        btnAdm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();
                entradaTransacao = new EntradaTransacao(Operacoes.ADMINISTRATIVA,String.valueOf(r.nextInt(100)));

                pay = new ElginPAY(entradaTransacao, handler, getContext());
                pay.start();

            }
        });

        btnCancelarVendas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final View dialogView = View.inflate(getContext(), R.layout.cancelamento, null);
                final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();


                Button data_time_set = dialogView.findViewById(R.id.btnOK);
                data_time_set.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Date date;
                        EditText txtData = dialogView.findViewById(R.id.txtData);
                        EditText txtHora = dialogView.findViewById(R.id.txtHora);
                        EditText txtNSU = dialogView.findViewById(R.id.txtNSU);
                        EditText txtValor = dialogView.findViewById(R.id.txtValor);

//                        String data = txtData.getText() + " " + txtHora.getText();
//                        data = data.replace("/", "");
//                        data = data.replace(".", "");
//                        data = data.replace(":", "");
//                        data = data.replace("-", "");
//                        try {
//                            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy hhmmss");
//                            date = sdf.parse(data);
//
//                        }catch (ParseException e){
//                            Toast.makeText(getContext(),"Erro ao converter data", Toast.LENGTH_LONG).show();
//                            return;
//                        }

                        Random r = new Random();

                        entradaTransacao = new EntradaTransacao(Operacoes.CANCELAMENTO, String.valueOf(r.nextInt(100)));
//                        entradaTransacao.informaValorTotal(txtValor.getText().toString());
//                        entradaTransacao.informaNsuTransacaoOriginal(txtNSU.getText().toString());
//                        entradaTransacao.informaDataHoraTransacaoOriginal(date);

                        pay = new ElginPAY(entradaTransacao, handler, getContext());
                        pay.start();

                    }});
                alertDialog.setView(dialogView);
                alertDialog.show();

            }
        });

//        btnConsultar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                entradaTransacao = new EntradaTransacao(Operacoes.)
//            }
//        });
//
//        btnReimpressao.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                entradaTransacao = new EntradaTransacao(Operacoes.)
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
        caixa = getCaixa(getContext());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
