package com.example.pdv.Home.Operacoes.PDV;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.example.pdv.R;
import com.example.pdv.Classes.Venda;
import com.example.pdv.Classes.Caixa;

import java.util.ArrayList;


public class VendasEfetuadasActivity extends AppCompatActivity {

    private static Caixa caixa; // in --> PDV Activity
    private static ArrayList<Venda> vendasEfetuadas; // in --> PDV Activity

    private ListView listView;
    private ArrayList<String> list;
    private CustomArrayAdapterVendasEfetuadas adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendas_efetuadas);

        // RECUPERA CAIXA E VENDAS EFETUADAS
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        caixa = ib.getParcelable("caixa");
        vendasEfetuadas = ib.getParcelableArrayList("vendasEfetuadas");

        // MONTA LIST VIEW COM AS VENDAS EFETUADAS...
        listView = findViewById(R.id.listview);
        list = new ArrayList<>();

        for (int i=0; i<vendasEfetuadas.size(); i++){
            list.add(vendasEfetuadas.get(i).getCod() + "|" +
                    vendasEfetuadas.get(i).getData().toString() + "|" +
                    vendasEfetuadas.get(i).getHora().toString() + "|" +
                    vendasEfetuadas.get(i).getValor()
            );
        }

        adapter = new CustomArrayAdapterVendasEfetuadas(list, this);
        listView.setAdapter(adapter);
    }

    // GETTER E SETTER DE caixa E vendasEfetuadas, ACESSADOS EM CustomArrayAdapterVendasEfetuadas
    public static Caixa getCaixa() { return caixa; }
    public static void setCaixa(Caixa cx) { caixa = cx; }

    public static ArrayList<Venda> getVendasEfetuadas() { return vendasEfetuadas; }
    public static void setVendasEfetuadas(ArrayList<Venda> ve) { vendasEfetuadas = ve; }
}
