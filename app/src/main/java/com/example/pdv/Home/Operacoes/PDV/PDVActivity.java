package com.example.pdv.Home.Operacoes.PDV;


import com.example.pdv.R;
import com.example.pdv.Classes.Data;
import com.example.pdv.Classes.Hora;
import com.example.pdv.Classes.Grupo;
import com.example.pdv.Classes.Produto;
import com.example.pdv.Classes.ProdutoVenda;
import com.example.pdv.Classes.Venda;
import com.example.pdv.Classes.Caixa;
import com.example.pdv.SQLite.DAO;

import static com.example.pdv.Utils.*;
import static com.example.pdv.GetClasses.*;
import static com.example.pdv.Classes.Produto.*;
import static com.example.pdv.SQLite.PostContract.*;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListView;
import android.app.AlertDialog;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.elgin.e1.Scanner.*;


public class PDVActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int INTENT_ADD_PRODUTOS = 1;
    private static final int INTENT_SCANNER = 2;
    private static final int INTENT_PAGAMENTO = 3;
    private static final int INTENT_BUSCAR = 4;
    private static final int INTENT_VENDAS_PENDENTES = 5;

    private static final String ARG_CAIXA = "caixa";
    private static final String ARG_GRUPOS = "grupos";
    private static final String ARG_PRODUTOS = "produtos";
    private static final String ARG_VENDA = "venda";
    private static final String ARG_VE = "vendasEfetuadas";
    private static final String ARG_VP = "vendasPendentes";

    private final DAO bd = new DAO();
    private ContentValues values = new ContentValues();

    private Caixa caixa; // in --> Operacoes Fragment ; out --> VendasEfetuadas Activity
    private int codProduto; // in --> AddProduto Activity

    private ArrayList<Grupo> grupos; // out --> AddProduto Activity
    private ArrayList<Produto> produtos; // out --> AddProduto Activity

    private ArrayList<Venda> vendasEfetuadas; // out --> VendasEfetuadas Activity
    private ArrayList<Venda> vendasPendentes; // out --> VendasPendentes Activity

    private static Venda venda; // in/out --> Pagamento Activity
    private int codVenda; // in --> VendasPendentes Activity
    private ArrayList<ProdutoVenda> aprodv; // Auxilia venda --> produtos

    private int numItens;
    private double valTot;

    private Intent intentAddProduto, intentPagamento;
    private Intent intentBuscar, intentVendasEfetuadas, intentVendasPendentes;
    private AlertDialog alerta;
    private AlertDialog.Builder builder;

    private TextView textViewNumItensValue, textViewValTotValue;
    private FloatingActionButton fab, fab1, fab2, fab3;
    private boolean isFABOpen;

    private ListView listView;
    private ArrayList<String> cupom;
    private CustomArrayAdapterPDV adapter;

    private Bundle ob;
    private NumberFormat numFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdv);

        // RECUPERA CAIXA
        Intent in = getIntent();
        Bundle ib = in.getExtras();
        caixa = ib.getParcelable("caixa");

        // OBTEM GRUPOS E PRODUTOS
        grupos = getGrupos(getApplicationContext());
        produtos = getProdutos(getApplicationContext());

        // INTENTS
        intentAddProduto = new Intent(getApplicationContext(), AddProdutoActivity.class);
        intentPagamento = new Intent(getApplicationContext(), PagamentoActivity.class);
        intentBuscar = new Intent(getApplicationContext(), BuscarActivity.class);
        intentVendasEfetuadas = new Intent(getApplicationContext(), VendasEfetuadasActivity.class);
        intentVendasPendentes = new Intent(getApplicationContext(), VendasPendentesActivity.class);

        // BARRA SUPERIOR
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // BARRA LATERAL
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // F.A.B.
        fab = findViewById(R.id.fab); // Abre/fecha menu de FABs
        fab1 = findViewById(R.id.fab1); // Add produto
        fab2 = findViewById(R.id.fab2); // Scanner
        fab3 = findViewById(R.id.fab3); // Pagar/fechar venda

        // LIST VIEW
        listView = findViewById(R.id.listview);

        // FAB PRINCIPAL
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        // FAB ADD PRODUTO
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (caixa == null) {
                    alerta = makeAlert(PDVActivity.this, 1);
                    alerta.show();
                } else if (grupos == null || produtos == null){
                    alerta = makeAlert(PDVActivity.this, 2);
                    alerta.show();
                } else {
                    ob = new Bundle();
                    ob.putParcelableArrayList(ARG_GRUPOS, grupos);
                    ob.putParcelableArrayList(ARG_PRODUTOS, produtos);

                    intentAddProduto.putExtras(ob);
                    startActivityForResult(intentAddProduto, INTENT_ADD_PRODUTOS);
                }
            }
        });

        // FAB SCANNER
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (caixa == null) {
                    alerta = makeAlert(PDVActivity.this, 1);
                    alerta.show();
                }else if (grupos == null || produtos == null){
                    alerta = makeAlert(PDVActivity.this, 2);
                    alerta.show();
                }else {
                    startActivityForResult(
                            Scanner.getScanner(PDVActivity.this), INTENT_SCANNER
                    );
                }
            }
        });

        // FAB PAGAR/FECHAR VENDA
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cupom.size() == 0){
                    alerta = makeAlert(PDVActivity.this, 3);
                    alerta.show();
                }else {
                    ob = new Bundle();
                    ob.putParcelable(ARG_VENDA, venda);

                    intentPagamento.putExtras(ob);
                    startActivityForResult(intentPagamento, INTENT_PAGAMENTO);
                }
            }
        });

        // LISTENER DO listView (ALTERAR PRODUTO DA VENDA)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // TODO --> alterar qtde e desconto
            }
        });

        // DEMAIS ELEMENTOS
        textViewNumItensValue = findViewById(R.id.textViewNumItensValue);
        textViewValTotValue = findViewById(R.id.textViewValTotValue);
        numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }

    @Override
    public void onResume(){
        super.onResume();

        // ATUALIZA ARRAYS DE VENDAS
        vendasEfetuadas = getVendasEfetuadas(getApplicationContext(), caixa==null?0:caixa.getCod());
        vendasPendentes = getVendasPendentes(getApplicationContext());

        // DETERMINA VENDA A SER EXIBIDA E MONTA O CUPOM
        if (codVenda == 0) {
            venda = null;
            codVenda = 0;
            aprodv = null;

            numItens = 0;
            valTot = 0;

            codProduto = 0;
            cupom = new ArrayList<>();
        } else {
            getVenda(codVenda);
        }

        // LIST VIEW
        adapter = new CustomArrayAdapterPDV(cupom, this);
        adapter.setTextViewNumItensValue(textViewNumItensValue);
        adapter.setTextViewValTotValue(textViewValTotValue);
        listView.setAdapter(adapter);

        // INFORMA VALORES
        textViewNumItensValue.setText(Integer.toString(numItens));
        textViewValTotValue.setText(numFormat.format(valTot));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case INTENT_ADD_PRODUTOS: {
                if (resultCode == 1) {
                    // OBTEM PRODUTO E ADICIONA AO CUPOM
                    codProduto = data.getIntExtra("produto", 0);

                    // ADICIONA PRODUTO NA VENDA ATUAL
                    if (codVenda == 0) { // Nenhuma venda carregada
                        if (codProduto != 0) { // Adicionado um produto --> cria nova venda
                            addProdNovaVenda();
                        }
                    } else { // Adiciona produto na venda atual
                        addProd();
                    }
                }
                break;
            } case INTENT_SCANNER: {
                if (resultCode == 2){ // SCANNER DEVE RETORNAR resultCode #2

                    String[] result = data.getStringArrayExtra("result");
                    CharSequence cs;

                    if (result[0].equals("1")) { // LEITURA BEM-SUCEDIDA

                        // PROCURA PRODUTO COM EAN LIDO...
                        codProduto = ean2cod(getApplicationContext(), result[1]);

                        if (codProduto == 0){ // PRODUTO NAO ENCONTRADO

                            cs = "Nenhum produto com o código " + result[1] + " foi encontrado.";
                            Toast.makeText(PDVActivity.this, cs, Toast.LENGTH_SHORT).show();

                        } else { // ADICIONA PRODUTO NA VENDA

                            // ADICIONA PRODUTO NA VENDA ATUAL
                            if (codVenda == 0) { // Nenhuma venda carregada
                                addProdNovaVenda(); // Adicionado um produto --> cria nova venda
                            } else { // Adiciona produto na venda atual
                                addProd();
                            }
                        }

                    } else { // ERRO OU LEITURA CANCELADA
                        cs = "Saída # " + result[0] + " na leitura do código.";
                        Toast.makeText(PDVActivity.this, cs, Toast.LENGTH_SHORT).show();
                    }

                } else { // SCANNER ENCERRADO
                    Toast.makeText(PDVActivity.this, "Scanner encerrado.", Toast.LENGTH_SHORT).show();
                }
                break;
            } case INTENT_PAGAMENTO: {
                if (resultCode == 1) {
                    // OBTEM PAGAMENTOS DA VENDA
                    venda = data.getParcelableExtra("venda");

                    fecharVenda();
                }
                break;
            } case INTENT_VENDAS_PENDENTES: {
                if (resultCode == 1) {
                    // OBTEM CODIGO DA VENDA PENDENTE SELECIONADA
                    codVenda = data.getIntExtra("codVenda", 0);
                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.pdv, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_buscar) {
            startActivity(intentBuscar);
        } else if (id == R.id.nav_salvar_venda) {
            if (codVenda == 0){
                alerta = makeAlert(PDVActivity.this, 10);
                alerta.show();
            } else {
                codVenda = 0; // Limpa tela inicial
                onResume();
            }
        } else if (id == R.id.nav_cancelar_venda) {
            if (codVenda == 0){
                alerta = makeAlert(PDVActivity.this, 8);
                alerta.show();
            } else {
                builder = new AlertDialog.Builder(PDVActivity.this);

                builder.setTitle("Cancelar venda");
                builder.setMessage("Deseja cancelar esta venda?");

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        cancelarVenda();
                    }
                });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // Nao faz nada
                    }
                });

                // OBTEM CONFIRMACAO DO USUARIO...
                alerta = builder.create();
                alerta.show();
            }
        } else if (id == R.id.nav_vendas_efetuadas) {
            if (caixa == null) {
                alerta = makeAlert(PDVActivity.this, 13);
                alerta.show();
            } else if (vendasEfetuadas == null || vendasEfetuadas.size() == 0){
                alerta = makeAlert(PDVActivity.this, 12);
                alerta.show();
            } else {
                ob = new Bundle();
                ob.putParcelable(ARG_CAIXA, caixa);
                ob.putParcelableArrayList(ARG_VE, vendasEfetuadas);

                intentVendasEfetuadas.putExtras(ob);
                startActivity(intentVendasEfetuadas);
            }
        } else if (id == R.id.nav_vendas_pendentes) {
            if (caixa == null) {
                alerta = makeAlert(PDVActivity.this, 13);
                alerta.show();
            } else if (vendasPendentes == null || vendasPendentes.size() == 0){
                alerta = makeAlert(PDVActivity.this, 11);
                alerta.show();
            } else {
                ob = new Bundle();
                ob.putParcelableArrayList(ARG_VP, vendasPendentes);

                intentVendasPendentes.putExtras(ob);
                startActivityForResult(intentVendasPendentes, INTENT_VENDAS_PENDENTES);
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showFABMenu(){
        isFABOpen = true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
        fab3.animate().translationY(-getResources().getDimension(R.dimen.standard_180));
        //fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_close));
        fab.setImageResource(R.drawable.ic_close);
    }

    private void closeFABMenu(){
        isFABOpen = false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);
        //fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_add));
        fab.setImageResource(R.drawable.ic_add);
    }

    private void getVenda(int cod){
        int flag = 0;

        for (int i = 0; i < vendasPendentes.size(); i++){
            if (vendasPendentes.get(i).getCod() == cod){
                venda = vendasPendentes.get(i);
                flag = 1;
                break;
            }
        }

        if (flag == 1) {
            codVenda = venda.getCod();
            aprodv = venda.getProdutos();

            numItens = 0;
            valTot = 0;

            codProduto = 0;
            cupom = new ArrayList<>();

            int codProd;
            for (int i = 0; i < aprodv.size(); i++) {
                codProd = aprodv.get(i).getCodProd();

                numItens += aprodv.get(i).getQtde();
                valTot = arithmetic(1, -1, -1, valTot,
                        arithmetic(3, -1, -1, aprodv.get(i).getQtde(),
                                arithmetic(2, -1, -1, cod2prv(getApplicationContext(), codProd),
                                        aprodv.get(i).getDesconto()
                                )
                        )
                ); // valTot += getQtde() * (cod2prv() - getDesconto())

                cupom.add(
                        aprodv.get(i).getQtde() + "|" +
                        cod2nom(getApplicationContext(), codProd) + "|" +
                        cod2prv(getApplicationContext(), codProd) + "|" +
                        aprodv.get(i).getDesconto() + "|" +
                        arithmetic(3, -1, -1, aprodv.get(i).getQtde(),
                                arithmetic(2, -1, -1, cod2prv(getApplicationContext(), codProd),
                                        aprodv.get(i).getDesconto()
                                )
                        ) // getQtde() * (cod2prv() - getDesconto())
                );
            }
        }
    }

    private void addProdNovaVenda(){
        codVenda = ++Venda.NUM_VENDAS;

        // CRIA VENDA
        venda = new Venda(codVenda, new Data(), new Hora());
        venda.setValor(
                arithmetic(1, -1, -1, venda.getValor(), cod2prv(getApplicationContext(), codProduto))
        ); // venda.getValor() + cod2prv(context, codProduto)

        // ADICIONA PRODUTO AA VENDA
        aprodv = new ArrayList<>();
        aprodv.add(new ProdutoVenda(codProduto, codVenda, 1, 0));
        venda.setProdutos(aprodv);

        // SALVA INFORMACOES NO BANCO DE DADOS...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // ADICIONA VENDA AAS VENDAS PENDENTES E AO CAIXA ATUAL
        // VENDAS PENDENTES --> ATUALIZADAS EM onResume();
        // CAIXA ATUAL --> ATUALIZADO AQUI

        // VENDA
        values.clear();
        values.put(VENDAS.COLUMN_COD, venda.getCod());
        values.put(VENDAS.COLUMN_DATA, venda.getData().toString());
        values.put(VENDAS.COLUMN_HORA, venda.getHora().toString());
        values.put(VENDAS.COLUMN_VALOR, venda.getValor());
        values.put(VENDAS.COLUMN_ABERTA, venda.isAberta());

        bd.OPEN(getApplicationContext());
        bd.INSERT(VENDAS.TABLE_NAME, null, values);
        bd.CLOSE();

        // PRODUTOS_VENDA
        values.clear();
        values.put(PRODUTOS_VENDAS.COLUMN_CODPROD, codProduto);
        values.put(PRODUTOS_VENDAS.COLUMN_CODVENDA, codVenda);
        values.put(PRODUTOS_VENDAS.COLUMN_QTDE, 1);
        values.put(PRODUTOS_VENDAS.COLUMN_DESC, 0);

        bd.OPEN(getApplicationContext());
        bd.INSERT(PRODUTOS_VENDAS.TABLE_NAME, null, values);
        bd.CLOSE();

        // VENDAS_CAIXA
        values.clear();
        values.put(VENDAS_CAIXAS.COLUMN_CODVENDA, codVenda);
        values.put(VENDAS_CAIXAS.COLUMN_CODCAIXA, caixa.getCod());

        bd.OPEN(getApplicationContext());
        bd.INSERT(VENDAS_CAIXAS.TABLE_NAME, null, values);
        bd.CLOSE();
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // ATUALIZA CAIXA
        caixa = getCaixa(getApplicationContext());
    }

    private void addProd(){
        // ATUALIZA VALOR DA VENDA
        venda.setValor(
                arithmetic(1, -1, -1, venda.getValor(), cod2prv(getApplicationContext(), codProduto))
        ); // venda.getValor() + cod2prv()

        // ADICIONA PRODUTO AA VENDA
        aprodv.add(new ProdutoVenda(codProduto, codVenda, 1, 0));
        venda.setProdutos(aprodv);

        // SALVA INFORMACOES NO BANCO DE DADOS...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // VENDA
        values.clear();
        values.put(VENDAS.COLUMN_VALOR, venda.getValor());

        bd.OPEN(getApplicationContext());
        bd.UPDATE(VENDAS.TABLE_NAME, values, VENDAS.COLUMN_COD + " = ?", new String[]{Integer.toString(codVenda)});
        bd.CLOSE();

        // PRODUTOS_VENDA
        values.clear();
        values.put(PRODUTOS_VENDAS.COLUMN_CODPROD, codProduto);
        values.put(PRODUTOS_VENDAS.COLUMN_CODVENDA, codVenda);
        values.put(PRODUTOS_VENDAS.COLUMN_QTDE, 1);
        values.put(PRODUTOS_VENDAS.COLUMN_DESC, 0);

        bd.OPEN(getApplicationContext());
        bd.INSERT(PRODUTOS_VENDAS.TABLE_NAME, null, values);
        bd.CLOSE();
        ////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    private void fecharVenda(){
        // FECHA VENDA
        venda.setCancelada(false);
        venda.fechar();

        // ATUALIZA CAIXA
        caixa.setVendaBruta(
                arithmetic(1, -1, -1, caixa.getVendaBruta(), venda.getValor())
        ); // caixa.getVendaBruta() + venda.getValor()

        // SALVA INFORMACOES NO BANCO DE DADOS...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // ATUALIZA DADOS DO CAIXA ATUAL
        // VENDAS EFETUADAS --> ATUALIZADAS EM onResume();
        // CAIXA ATUAL --> ATUALIZADO AQUI

        // VENDA
        values.clear();
        values.put(VENDAS.COLUMN_ABERTA, venda.isAberta());
        values.put(VENDAS.COLUMN_CANCELADA, venda.isCancelada());
        // CAMPOS COD, DATA, HORA --> ABERTURA DA VENDA
        // CAMPO VALOR --> ADICAO/REMOCAO DE PRODUTOS
        // CAMPOS ABERTA, CANCELADA --> FECHAMENTO DA VENDA

        bd.OPEN(getApplicationContext());
        bd.UPDATE(VENDAS.TABLE_NAME, values, VENDAS.COLUMN_COD + " = ?", new String[]{Integer.toString(codVenda)});
        bd.CLOSE();

        // PAGAMENTO

        /* TODO
            No futuro, analisar o meio de pagamento utilizado,
            bem como o numero de parcelas, caso Cartao de Credito */

        for (int i=0; i<venda.getPagamentos().size(); i++){
            values.clear();
            values.put(PAGAMENTOS.COLUMN_CODVENDA, venda.getPagamentos().get(i).getCodVenda());
            values.put(PAGAMENTOS.COLUMN_FPGTO, venda.getPagamentos().get(i).getFpgto());
            values.put(PAGAMENTOS.COLUMN_VALOR, venda.getPagamentos().get(i).getValorPago());

            bd.OPEN(getApplicationContext());
            bd.INSERT(PAGAMENTOS.TABLE_NAME, null, values);
            bd.CLOSE();
        }

        // CAIXA
        values.clear();
        values.put(CAIXAS.COLUMN_VENDABRUTA, caixa.getVendaBruta());

        bd.OPEN(getApplicationContext());
        bd.UPDATE(CAIXAS.TABLE_NAME, values, CAIXAS.COLUMN_COD + " = ?", new String[]{Integer.toString(caixa.getCod())});
        bd.CLOSE();
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // ATUALIZA CAIXA
        caixa = getCaixa(getApplicationContext());

        // IMPRIME CUPOM...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        builder = new AlertDialog.Builder(PDVActivity.this);
        final Venda tmpVenda = venda;

        builder.setTitle("Imprimir cupom da venda");
        builder.setMessage("Deseja imprimir o cupom da venda?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                imprimeVenda(getApplicationContext(), tmpVenda);
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                // Nao faz nada
            }
        });

        // OBTEM CONFIRMACAO DO USUARIO...
        alerta = builder.create();
        alerta.show();
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        codVenda = 0; // Limpa tela inicial
    }

    private void cancelarVenda(){
        // CANCELA VENDA
        venda.setCancelada(true);

        // SALVA INFORMACOES NO BANCO DE DADOS...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // ATUALIZA DADOS DO CAIXA ATUAL
        // CAIXA ATUAL --> ATUALIZADO AQUI

        // VENDA
        values.clear();
        values.put(VENDAS.COLUMN_COD, venda.getCod());
        values.put(VENDAS.COLUMN_DATA, venda.getData().toString());
        values.put(VENDAS.COLUMN_HORA, venda.getHora().toString());
        values.put(VENDAS.COLUMN_VALOR, venda.getValor());
        values.put(VENDAS.COLUMN_ABERTA, venda.isAberta());
        values.put(VENDAS.COLUMN_CANCELADA, venda.isCancelada());

        bd.OPEN(getApplicationContext());
        bd.UPDATE(VENDAS.TABLE_NAME, values, VENDAS.COLUMN_COD + " = ?", new String[]{Integer.toString(codVenda)});
        bd.CLOSE();
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // ATUALIZA CAIXA
        caixa = getCaixa(getApplicationContext());

        alerta = makeAlert(PDVActivity.this, 9);
        alerta.show();

        codVenda = 0; // Limpa tela inicial
        onResume();
    }

    // GETTER E SETTER DE venda, ACESSADOS EM CustomArrayAdapterPDV
    public static Venda getVenda() { return venda; }
    public static void setVenda(Venda v) { venda = v; }
}
