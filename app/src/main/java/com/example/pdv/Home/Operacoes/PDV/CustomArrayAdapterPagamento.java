package com.example.pdv.Home.Operacoes.PDV;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pdv.R;
import static com.example.pdv.Utils.*;
import static com.example.pdv.Home.Operacoes.PDV.PagamentoActivity.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class CustomArrayAdapterPagamento extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list;
    private Context context;
    private NumberFormat numFormat;

    private TextView textViewSaldoValue, textViewSaldo;
    private String[] dados;

    CustomArrayAdapterPagamento(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
        this.numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_pagamento, null);
        }

        // MANIPULA TEXTVIEW DA LISTVIEW
        TextView listItemForma = view.findViewById(R.id.list_item_forma);
        TextView listItemValor = view.findViewById(R.id.list_item_valor);
        dados = list.get(position).split("\\|");

        listItemForma.setText(dados[0]);
        listItemValor.setText(numFormat.format(Double.parseDouble(dados[1])));

        // MANIPULA BOTAO REMOVER
        ImageView deleteBtn = view.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dados = list.get(position).split("\\|");
                list.remove(position);

                setSaldoTroco(arithmetic(1, -1, -1, getSaldoTroco(), Double.parseDouble(dados[1]))); // saldoTroco + dados[1]

                if (getSaldoTroco() < 0) {
                    textViewSaldo.setText(R.string.text_troco);
                    textViewSaldoValue.setText(numFormat.format(getSaldoTroco()*-1));
                }else{
                    textViewSaldo.setText(R.string.text_saldo);
                    textViewSaldoValue.setText(numFormat.format(getSaldoTroco()));
                }

                notifyDataSetChanged();
            }
        });

        return view;
    }

    void setTextViewSaldoValue(TextView textViewSaldoValue) { this.textViewSaldoValue = textViewSaldoValue; }
    void setTextViewSaldo(TextView textViewSaldo) { this.textViewSaldo = textViewSaldo; }
}
