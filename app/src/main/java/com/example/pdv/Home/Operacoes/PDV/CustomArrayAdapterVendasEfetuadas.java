package com.example.pdv.Home.Operacoes.PDV;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pdv.R;
import com.example.pdv.Classes.Venda;
import com.example.pdv.Classes.Caixa;
import com.example.pdv.SQLite.DAO;

import static com.example.pdv.Utils.*;
import static com.example.pdv.GetClasses.*;
import static com.example.pdv.Home.Operacoes.PDV.VendasEfetuadasActivity.*;
import static com.example.pdv.SQLite.PostContract.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class CustomArrayAdapterVendasEfetuadas extends BaseAdapter implements ListAdapter {

    private ArrayList<String> list;
    private Context context;
    private NumberFormat numFormat;

    private String[] dados;
    private Venda venda;
    private Caixa caixa;

    private AlertDialog alerta;
    private AlertDialog.Builder builder;

    private final DAO bd = new DAO();
    private ContentValues values = new ContentValues();

    CustomArrayAdapterVendasEfetuadas(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
        this.numFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_vendas_efetuadas, null);
        }

        // MANIPULA TEXTVIEW DA LISTVIEW
        TextView listItemCodVE = view.findViewById(R.id.list_item_cod_ve);
        TextView listItemDataberVE = view.findViewById(R.id.list_item_databer_ve);
        TextView listItemHoraberVE = view.findViewById(R.id.list_item_horaber_ve);
        TextView listItemValorVE = view.findViewById(R.id.list_item_valor_ve);
        dados = list.get(position).split("\\|");

        listItemCodVE.setText(dados[0]);
        listItemDataberVE.setText(dados[1]);
        listItemHoraberVE.setText(dados[2]);
        listItemValorVE.setText(numFormat.format(Double.parseDouble(dados[3])));

        // MANIPULA BOTOES IMPRIMIR E CANCELAR VENDA EFETUADA
        ImageView printBtn = view.findViewById(R.id.print_btn);
        ImageView cancelBtn = view.findViewById(R.id.cancel_btn);

        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Context c = v.getContext();
                imprimirVE(c, position);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Context c = v.getContext();
                builder = new AlertDialog.Builder(v.getContext());

                builder.setTitle("Cancelar venda");
                builder.setMessage("Deseja cancelar esta venda?");

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        cancelarVE(c, position);
                        notifyDataSetChanged();
                    }
                });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        // Nao faz nada
                    }
                });

                // OBTEM CONFIRMACAO DO USUARIO...
                alerta = builder.create();
                alerta.show();
            }
        });

        return view;
    }

    private void imprimirVE(Context context, int position){
        venda = getVendasEfetuadas().get(position);
        imprimeVenda(context, venda);
    }

    private void cancelarVE(Context context, int position){
        list.remove(position);

        caixa = getCaixa();
        venda = getVendasEfetuadas().get(position);

        // CANCELA VENDA
        venda.setCancelada(true);

        // ATUALIZA CAIXA
        caixa.setCancelamentos(
                arithmetic(1, -1, -1, caixa.getCancelamentos(), venda.getValor())
        ); // caixa.getCancelamentos() + venda.getValor()

        // SALVA INFORMACOES NO BANCO DE DADOS...
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // ATUALIZA DADOS DO CAIXA ATUAL
        // VENDAS EFETUADAS --> ATUALIZADAS AQUI
        // CAIXA ATUAL --> ATUALIZADO AQUI

        // VENDA
        values.clear();
        values.put(VENDAS.COLUMN_CANCELADA, venda.isCancelada());

        bd.OPEN(context);
        bd.UPDATE(VENDAS.TABLE_NAME, values, VENDAS.COLUMN_COD + " = ?", new String[]{Integer.toString(venda.getCod())});
        bd.CLOSE();

        // CAIXA
        values.clear();
        values.put(CAIXAS.COLUMN_CANCELAMENTOS, caixa.getCancelamentos());

        bd.OPEN(context);
        bd.UPDATE(CAIXAS.TABLE_NAME, values, CAIXAS.COLUMN_COD + " = ?", new String[]{Integer.toString(caixa.getCod())});
        bd.CLOSE();
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // ATUALIZA VENDAS EFETUADAS E CAIXA
        setVendasEfetuadas(getVendasEfetuadas(context, caixa.getCod()));
        caixa = getCaixa(context);

        alerta = makeAlert(context, 9);
        alerta.show();
    }
}
