package com.example.pdv;


import android.content.Context;
import java.util.ArrayList;

import com.example.pdv.Classes.Pagamento;
import com.example.pdv.SQLite.DAO;
import com.example.pdv.Classes.Data;
import com.example.pdv.Classes.Hora;
import com.example.pdv.Classes.Grupo;
import com.example.pdv.Classes.Produto;
import com.example.pdv.Classes.Venda;
import com.example.pdv.Classes.ProdutoVenda;
import com.example.pdv.Classes.Caixa;
import com.example.pdv.Classes.VendaCaixa;

import static com.example.pdv.Utils.*;
import static com.example.pdv.SQLite.PostContract.*;
import static com.example.pdv.Classes.Data.getData;
import static com.example.pdv.Classes.Hora.getHora;


/** CLASSE PARA MONTAR AS CLASSES DE NEGOCIO A PARTIR DO BANCO DE DADOS **/
public final class GetClasses {

    private static final DAO bd = new DAO();

    // VARIAVEIS PARA MANIPULACAO DOS DADOS/QUERIES
    private static String table, selection, orderBy;
    private static String[] columns, selectionArgs;
    private static String[][] RESULT;


    /** METODO PARA CONFIGURAR OS DADOS DO AMBIENTE **/
    public static final void setAmbiente(Context context){
        int numVendas, numCaixas;

        bd.OPEN(context);
        RESULT = bd.SELECT(VENDAS.TABLE_NAME, null, null, null, null, null, null);
        bd.CLOSE();
        numVendas = RESULT.length;

        bd.OPEN(context);
        RESULT = bd.SELECT(CAIXAS.TABLE_NAME, null, null, null, null, null, null);
        bd.CLOSE();
        numCaixas = RESULT.length;

        Venda.NUM_VENDAS = numVendas;
        Caixa.NUM_CAIXAS = numCaixas;
    }

    /** METODO PARA RECUPERAR OS GRUPOS CADASTRADOS (NULL = NENHUM) **/
    public static final ArrayList<Grupo> getGrupos(Context context){
        Grupo g;
        ArrayList<Grupo> grupos;

        table = GRUPOS.TABLE_NAME;
        columns = new String[]{
                GRUPOS._ID,
                GRUPOS.COLUMN_NOME
        };
        orderBy = GRUPOS.COLUMN_NOME;

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, null, null, null, null, orderBy);
        bd.CLOSE();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        if (RESULT.length == 0){ // NENHUM GRUPO CADASTRADO

            return null;

        } else { // RETORNA LISTA DE GRUPOS

            int cod;
            String nome;

            grupos = new ArrayList<>();
            for (String[] strings : RESULT) {
                cod = Integer.parseInt(strings[0]);
                nome = strings[1];

                g = new Grupo(nome);
                g.setCodigo(cod);

                grupos.add(g);
            }

            return grupos;
        }
    }

    /** METODO PARA RECUPERAR OS PRODUTOS CADASTRADOS (NULL = NENHUM) **/
    public static final ArrayList<Produto> getProdutos(Context context){
        Produto p;
        ArrayList<Produto> produtos;

        table = PRODUTOS.TABLE_NAME;
        columns = new String[]{
                PRODUTOS.COLUMN_COD,
                PRODUTOS.COLUMN_GRUPO,
                PRODUTOS.COLUMN_NOME,
                PRODUTOS.COLUMN_EAN,
                PRODUTOS.COLUMN_PRC_V,
                PRODUTOS.COLUMN_CUSTO,
                PRODUTOS.COLUMN_PERC_MAX_DESC
        };
        orderBy = PRODUTOS.COLUMN_NOME;

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, null, null, null, null, orderBy);
        bd.CLOSE();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        if (RESULT.length == 0){ // NENHUM PRODUTO CADASTRADO

            return null;

        } else { // RETORNA LISTA DE PRODUTOS

            int cod, grupo;
            String nome;
            double precoVenda;

            produtos = new ArrayList<>();
            for (String[] strings : RESULT) {
                cod = Integer.parseInt(strings[0]);
                grupo = Integer.parseInt(strings[1]);
                nome = strings[2];
                precoVenda = Double.parseDouble(strings[4]);

                p = new Produto(cod, grupo, nome, precoVenda);
                if (strings[3] != null) p.setEAN(strings[3]);
                if (strings[5] != null) p.setCusto(Double.parseDouble(strings[5]));
                if (strings[6] != null) p.setPercMaxDesc(Double.parseDouble(strings[6]));

                produtos.add(p);
            }

            return produtos;
        }
    }

    /** METODO PARA RETORNAR AS VENDAS EFETUADAS DE UM DADO CAIXA (NULL = NENHUM) **/
    public static final ArrayList<Venda> getVendasEfetuadas(Context context, int codCaixa){
        ProdutoVenda prodV;
        ArrayList<ProdutoVenda> produtosVendas;
        Pagamento pag;
        ArrayList<Pagamento> pagamentos;
        Venda v;
        ArrayList<Venda> vendas;

        table = VENDAS.TABLE_NAME + " INNER JOIN " + VENDAS_CAIXAS.TABLE_NAME + " ON " +
                VENDAS.COLUMN_COD + " = " + VENDAS_CAIXAS.COLUMN_CODVENDA + " AND " +
                VENDAS_CAIXAS.COLUMN_CODCAIXA + " = " + codCaixa;
        columns = new String[]{
                VENDAS.COLUMN_COD,
                VENDAS.COLUMN_DATA,
                VENDAS.COLUMN_HORA,
                VENDAS.COLUMN_VALOR,
                VENDAS.COLUMN_ABERTA,
                VENDAS.COLUMN_CANCELADA
        };
        selection = VENDAS.COLUMN_ABERTA + " = ?" + " AND " + VENDAS.COLUMN_CANCELADA + " = ?";
        selectionArgs = new String[]{"0", "0"};
        orderBy = VENDAS.COLUMN_COD;

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, orderBy);
        bd.CLOSE();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        if (RESULT.length == 0){ // NAO HA' VENDAS FECHADAS

            return null;

        } else { // HA' VENDAS FECHADAS

            int cod;
            Data data;
            Hora hora;

            vendas = new ArrayList<>();
            for (String[] strings : RESULT){
                cod = Integer.parseInt(strings[0]);
                data = getData(strings[1]);
                hora = getHora(strings[2]);

                v = new Venda(cod, data, hora);
                v.fechar();
                if (strings[3] != null) v.setValor(Double.parseDouble(strings[3]));
                if (strings[5] != null) v.setCancelada(intBoolean(strings[5])); // Deve ser sempre null...

                vendas.add(v);
            }

            // OBTEM OS PRODUTOS RELACIONADOS AAS VENDAS
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            table = PRODUTOS_VENDAS.TABLE_NAME;
            columns = new String[]{
                    PRODUTOS_VENDAS._ID,
                    PRODUTOS_VENDAS.COLUMN_CODPROD,
                    PRODUTOS_VENDAS.COLUMN_CODVENDA,
                    PRODUTOS_VENDAS.COLUMN_QTDE,
                    PRODUTOS_VENDAS.COLUMN_DESC
            };
            selection = PRODUTOS_VENDAS.COLUMN_CODVENDA + " = ?";

            for (int i=0; i<vendas.size(); i++) {
                selectionArgs = new String[]{Integer.toString(vendas.get(i).getCod())};

                bd.OPEN(context);
                RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
                bd.CLOSE();

                produtosVendas = new ArrayList<>();
                for (String[] strings : RESULT) {
                    prodV = new ProdutoVenda(Integer.parseInt(strings[1]),
                            Integer.parseInt(strings[2]),
                            Integer.parseInt(strings[3]),
                            Double.parseDouble(strings[4]));
                    prodV.setCodigo(Integer.parseInt(strings[0]));

                    produtosVendas.add(prodV);
                }

                vendas.get(i).setProdutos(produtosVendas);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // OBTEM OS PAGAMENTOS RELACIONADOS AAS VENDAS
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            table = PAGAMENTOS.TABLE_NAME;
            columns = new String[]{
                    PAGAMENTOS._ID,
                    PAGAMENTOS.COLUMN_CODVENDA,
                    PAGAMENTOS.COLUMN_FPGTO,
                    PAGAMENTOS.COLUMN_VALOR,
                    PAGAMENTOS.COLUMN_PARCELAS
            };
            selection = PAGAMENTOS.COLUMN_CODVENDA + " = ?";

            for (int i=0; i<vendas.size(); i++) {
                selectionArgs = new String[]{Integer.toString(vendas.get(i).getCod())};

                bd.OPEN(context);
                RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
                bd.CLOSE();

                pagamentos = new ArrayList<>();
                for (String[] strings : RESULT) {
                    pag = new Pagamento(Integer.parseInt(strings[1]),
                            Integer.parseInt(strings[2]),
                            Double.parseDouble(strings[3]));
                    pag.setCodigo(Integer.parseInt(strings[0]));
                    if (strings[4] != null) pag.setParcelas(Integer.parseInt(strings[4]));

                    pagamentos.add(pag);
                }

                vendas.get(i).setPagamentos(pagamentos);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            produtosVendas = null;
            pagamentos = null;
            return vendas;
        }
    }

    /** METODO PARA RETORNAR AS VENDAS PENDENTES (OU SALVAS; NULL = NENHUM) **/
    public static final ArrayList<Venda> getVendasPendentes(Context context){
        ProdutoVenda pv;
        ArrayList<ProdutoVenda> produtosVendas;
        Venda v;
        ArrayList<Venda> vendas;

        table = VENDAS.TABLE_NAME;
        columns = new String[]{
                VENDAS.COLUMN_COD,
                VENDAS.COLUMN_DATA,
                VENDAS.COLUMN_HORA,
                VENDAS.COLUMN_VALOR,
                VENDAS.COLUMN_ABERTA,
                VENDAS.COLUMN_CANCELADA
        };
        selection = VENDAS.COLUMN_ABERTA + " = ?" + " AND " + VENDAS.COLUMN_CANCELADA + " IS NULL";
        selectionArgs = new String[]{"1"};
        orderBy = VENDAS.COLUMN_COD;

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, orderBy);
        bd.CLOSE();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        if (RESULT.length == 0){ // NAO HA' VENDAS EM ABERTO

            return null;

        } else { // HA' VENDAS EM ABERTO

            int cod;
            Data data;
            Hora hora;

            vendas = new ArrayList<>();
            for (String[] strings : RESULT){
                cod = Integer.parseInt(strings[0]);
                data = getData(strings[1]);
                hora = getHora(strings[2]);

                v = new Venda(cod, data, hora);
                if (strings[3] != null) v.setValor(Double.parseDouble(strings[3]));
                if (strings[5] != null) v.setCancelada(intBoolean(strings[5])); // Deve ser sempre null...

                vendas.add(v);
            }

            // OBTEM OS PRODUTOS RELACIONADOS AAS VENDAS
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            table = PRODUTOS_VENDAS.TABLE_NAME;
            columns = new String[]{
                    PRODUTOS_VENDAS._ID,
                    PRODUTOS_VENDAS.COLUMN_CODPROD,
                    PRODUTOS_VENDAS.COLUMN_CODVENDA,
                    PRODUTOS_VENDAS.COLUMN_QTDE,
                    PRODUTOS_VENDAS.COLUMN_DESC
            };
            selection = PRODUTOS_VENDAS.COLUMN_CODVENDA + " = ?";

            for (int i=0; i<vendas.size(); i++) {
                selectionArgs = new String[]{Integer.toString(vendas.get(i).getCod())};

                bd.OPEN(context);
                RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
                bd.CLOSE();

                produtosVendas = new ArrayList<>();
                for (String[] strings : RESULT) {
                    pv = new ProdutoVenda(Integer.parseInt(strings[1]),
                            Integer.parseInt(strings[2]),
                            Integer.parseInt(strings[3]),
                            Double.parseDouble(strings[4]));
                    pv.setCodigo(Integer.parseInt(strings[0]));

                    produtosVendas.add(pv);
                }

                vendas.get(i).setProdutos(produtosVendas);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            produtosVendas = null;
            return vendas;
        }
    }

    /** METODO PARA RECUPERAR O CAIXA ATUAL. RETORNA NULL SE NAO HOUVER NENHUM **/
    public static final Caixa getCaixa(Context context){
        Venda v;
        ArrayList<Venda> vendas;
        ArrayList<VendaCaixa> vendasCaixas;
        Caixa cx;

        table = CAIXAS.TABLE_NAME;
        columns = new String[]{
                CAIXAS.COLUMN_COD,
                CAIXAS.COLUMN_DATABER,
                CAIXAS.COLUMN_HORABER,
                CAIXAS.COLUMN_VALORABER,
                CAIXAS.COLUMN_DATAFECH,
                CAIXAS.COLUMN_HORAFECH,
                CAIXAS.COLUMN_VENDABRUTA,
                CAIXAS.COLUMN_CANCELAMENTOS,
                CAIXAS.COLUMN_ABERTO
        };
        selection = CAIXAS.COLUMN_ABERTO + " = ?";
        selectionArgs = new String[]{"1"};

        bd.OPEN(context);
        RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
        bd.CLOSE();

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        if (RESULT.length == 0) { // NAO HA' CAIXA ABERTO

            return null;

        } else { // HA' UM CAIXA ABERTO

            int cod = Integer.parseInt(RESULT[0][0]);
            Data data = getData(RESULT[0][1]);
            Hora hora = getHora(RESULT[0][2]);
            double valorAbertura = Double.parseDouble(RESULT[0][3]);

            cx = new Caixa(cod, data, hora, valorAbertura);
            if (RESULT[0][4] != null) cx.setDataFechamento(getData(RESULT[0][4]));
            if (RESULT[0][5] != null) cx.setHoraFechamento(getHora(RESULT[0][5]));
            if (RESULT[0][6] != null) cx.setVendaBruta(Double.parseDouble(RESULT[0][6]));
            if (RESULT[0][7] != null) cx.setCancelamentos(Double.parseDouble(RESULT[0][7]));

            // OBTEM AS VENDAS RELACIONADAS AO CAIXA
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            vendasCaixas = new ArrayList<>();

            table = VENDAS_CAIXAS.TABLE_NAME;
            columns = new String[]{
                    VENDAS_CAIXAS.COLUMN_CODVENDA,
                    VENDAS_CAIXAS.COLUMN_CODCAIXA
            };
            selection = VENDAS_CAIXAS.COLUMN_CODCAIXA + " = ?";
            selectionArgs = new String[]{Integer.toString(cod)};

            bd.OPEN(context);
            RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
            bd.CLOSE();

            for (String[] strings : RESULT) {
                vendasCaixas.add(new VendaCaixa(Integer.parseInt(strings[0]), Integer.parseInt(strings[1])));
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            // OBTEM OS DADOS DAS VENDAS ENCONTRADAS
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            vendas = new ArrayList<>();

            table = VENDAS.TABLE_NAME;
            columns = new String[]{
                    VENDAS.COLUMN_COD,
                    VENDAS.COLUMN_DATA,
                    VENDAS.COLUMN_HORA,
                    VENDAS.COLUMN_VALOR,
                    VENDAS.COLUMN_ABERTA,
                    VENDAS.COLUMN_CANCELADA
            };
            selection = VENDAS.COLUMN_COD + " = ?";

            for (int i=0; i<vendasCaixas.size(); i++) {
                selectionArgs = new String[]{Integer.toString(vendasCaixas.get(i).getCodVenda())};

                bd.OPEN(context);
                RESULT = bd.SELECT(table, columns, selection, selectionArgs, null, null, null);
                bd.CLOSE();

                v = new Venda(Integer.parseInt(RESULT[0][0]), getData(RESULT[0][1]), getHora(RESULT[0][2]));
                if (RESULT[0][3] != null) v.setValor(Double.parseDouble(RESULT[0][3]));
                if (RESULT[0][4].equals("0")) v.fechar();
                if (RESULT[0][5] != null) v.setCancelada(intBoolean(RESULT[0][5]));

                vendas.add(v);
            }

            cx.setVendas(vendas);
            ////////////////////////////////////////////////////////////////////////////////////////////////////

            vendasCaixas = null;
            vendas = null;

            return cx;
        }
    }


    /** METODO PARA RETORNAR AS VENDAS EFETUADAS EM UM DADO PERIODO (NULL = NENHUM) **/
    public static final ArrayList<Venda> getVendasPerioro(Context context, Data inicio, Data fim){
        return null;
    }

    /** METODO PARA RETORNAR OS CAIXAS ABERTOS EM UM DADO PERIODO (NULL = NENHUM) **/
    public static final ArrayList<Caixa> getCaixasPeriodo(Context context, Data inicio, Data fim){
        return null;
    }
}
